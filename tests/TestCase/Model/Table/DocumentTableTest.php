<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DocumentTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DocumentTable Test Case
 */
class DocumentTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DocumentTable
     */
    public $Document;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.document',
        'app.products'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Document') ? [] : ['className' => DocumentTable::class];
        $this->Document = TableRegistry::get('Document', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Document);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
