<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RecrutersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RecrutersTable Test Case
 */
class RecrutersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RecrutersTable
     */
    public $Recruters;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.recruters'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Recruters') ? [] : ['className' => RecrutersTable::class];
        $this->Recruters = TableRegistry::get('Recruters', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Recruters);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
