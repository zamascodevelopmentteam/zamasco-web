<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Recruter Entity
 *
 * @property int $id
 * @property string $job_title
 * @property string $name
 * @property string $phone
 * @property string $address
 * @property string $education
 * @property string $file
 * @property string $file_dir
 * @property \Cake\I18n\FrozenTime $created
 */
class Recruter extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'job_title' => true,
        'name' => true,
        'phone' => true,
        'address' => true,
        'education' => true,
        'file' => true,
        'file_dir' => true,
        'created' => true
    ];
}
