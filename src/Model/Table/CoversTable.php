<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Covers Model
 *
 * @method \App\Model\Entity\Cover get($primaryKey, $options = [])
 * @method \App\Model\Entity\Cover newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Cover[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Cover|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Cover patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Cover[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Cover findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CoversTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('covers');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'picture' => [
                'path' => 'webroot/assets/picture/{model}/{year}/{month}/',
                'fields' => [
                    'dir' => 'picture_dir', // defaults to `dir`
                ],
                // 'transformer' =>  function ($table, $entity, $data, $field, $settings) {
                //     $extension = pathinfo($data['name'], PATHINFO_EXTENSION);

                //     // Store the thumbnail in a temporary file
                //     $tmp = tempnam(sys_get_temp_dir(), 'upload') . '.' . $extension;

                //     // Use the Imagine library to DO THE THING
                //     $size = new \Imagine\Image\Box(100, 100);
                //     $mode = \Imagine\Image\ImageInterface::THUMBNAIL_INSET;
                //     $imagine = new \Imagine\Gd\Imagine();

                //     // Save that modified file to our temp file
                //     $imagine->open($data['tmp_name'])
                //         ->thumbnail($size, $mode)
                //         ->save($tmp);

                //     // Now return the original *and* the thumbnail
                //     return [
                //         $data['tmp_name'] => $data['name'],
                //         $tmp => 'thumbnail-' . $data['name'],
                //     ];
                // },
                'nameCallback' => function ($table, $entity, $data, $field, $settings) {
                    return strtolower($data['name']);
                },
                'deleteCallback' => function ($path, $entity, $field, $settings) {
                    // When deleting the entity, both the original and the thumbnail will be removed
                    // when keepFilesOnDelete is set to false
                    return [
                        $path . $entity->{$field},
                        $path . 'thumbnail-' . $entity->{$field}
                    ];
                },
                'keepFilesOnDelete' => false
            ],
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('title')
            ->requirePresence('title', 'create')
            ->allowEmpty('title');

        $validator
            ->scalar('pages')
            ->maxLength('pages', 255)
            ->requirePresence('pages', 'create')
            ->notEmpty('pages');

        $validator
            ->scalar('text_content')
            ->maxLength('text_content', 255)
            ->requirePresence('text_content', 'create')
            ->allowEmpty('text_content');

        $validator
            ->scalar('animation')
            ->maxLength('animation', 255)
            ->requirePresence('animation', 'create')
            ->allowEmpty('animation');
            
        $validator
            ->integer('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        return $validator;
    }
}
