<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Recruters Model
 *
 * @method \App\Model\Entity\Recruter get($primaryKey, $options = [])
 * @method \App\Model\Entity\Recruter newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Recruter[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Recruter|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Recruter patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Recruter[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Recruter findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RecrutersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('recruters');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'file' => [
                'path' => 'webroot/assets/file/{model}/{year}/{month}/',
                'fields' => [
                    'dir' => 'file_dir', // defaults to `dir`
                ],
                'nameCallback' => function ($table, $entity, $data, $field, $settings) {
                    return date('d-m-Y_H-i-s__').strtolower($data['name']);
                },
                'deleteCallback' => function ($path, $entity, $field, $settings) {
                    // When deleting the entity, both the original and the thumbnail will be removed
                    // when keepFilesOnDelete is set to false
                    return [
                        $path . $entity->{$field},
                    ];
                },
                'keepFilesOnDelete' => false
            ],
        ]);

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('job_title')
            ->maxLength('job_title', 255)
            ->requirePresence('job_title', 'create')
            ->notEmpty('job_title');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->scalar('phone')
            ->maxLength('phone', 255)
            ->requirePresence('phone', 'create')
            ->notEmpty('phone');

        $validator
            ->scalar('address')
            ->requirePresence('address', 'create')
            ->notEmpty('address');

        $validator
            ->scalar('education')
            ->maxLength('education', 255)
            ->requirePresence('education', 'create')
            ->notEmpty('education');

        // $validator
        //     ->scalar('file')
        //     ->maxLength('file', 255)
        //     ->allowEmpty('file');

        // $validator
        //     ->scalar('file_dir')
        //     ->maxLength('file_dir', 255)
        //     ->allowEmpty('file_dir');

        return $validator;
    }
}
