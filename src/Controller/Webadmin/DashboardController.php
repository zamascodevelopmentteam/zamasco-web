<?php
namespace App\Controller\Webadmin;

use App\Controller\AppController;

class DashboardController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        if(php_sapi_name() !== 'cli'){
            $this->Auth->allow(['index']);
        }
        
    }

    function beforeFilter(\Cake\Event\Event $event){
        if (!$this->Auth->user()) {
            $this->redirect($this->Auth->logout());
        }
    }

    public function index()
    {
        
    }

    public function redi()
    {
        $this->redirect(['action'=>'index']);
    }
}