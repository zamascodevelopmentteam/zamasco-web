<?php

namespace App\Controller\Webadmin;

use App\Controller\AppController;
use Cake\Routing\Router;

/**
 * Products Controller
 *
 * @property \App\Model\Table\ProductsTable $Products
 *
 * @method \App\Model\Entity\Product[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProductsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        if (php_sapi_name() !== 'cli') {
            $this->Auth->allow(['index']);
        }
    }

    function beforeFilter(\Cake\Event\Event $event)
    {
        parent::beforeFilter($event);

        if (isset($this->Security) && $this->request->isAjax() && ($this->action = 'index' || $this->action = 'delete')) {

            $this->Security->config('validatePost', false);
            //$this->getEventManager()->off($this->Csrf);

        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if ($this->request->is('ajax')) {
            $source = $this->Products;
            $searchAble = [
                'Products.id',
                'Products.name',
                'Products.division',
                'Products.category'
            ];
            $data = [
                'source' => $source,
                'searchAble' => $searchAble,
                'defaultField' => 'Products.id',
                'defaultSort' => 'desc',
                'defaultSearch' => [
                    // [
                    //     'keyField' => 'group_id',
                    //     'condition' => '=',
                    //     'value' => 1
                    // ]
                ],
                // 'contain' => ['Groups']

            ];
            $asd   = $this->Datatables->make($data);
            //$this->set('data', $asd);
            $data = $asd['data'];
            $meta = $asd['meta'];
            $this->set('data', $data);
            $this->set('meta', $meta);
            $this->set('_serialize', ['data', 'meta']);
        } else {
            $titleModule = "Products";
            $titlesubModule = "List Products";
            $breadCrumbs = [
                Router::url(['action' => 'index']) => $titlesubModule
            ];
            $this->set(compact('titleModule', 'breadCrumbs', 'titlesubModule'));
        }
    }

    /**
     * View method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $product = $this->Products->get($id, [
            'contain' => []
        ]);

        $this->set('product', $product);
        $titleModule = "Product";
        $titlesubModule = "View " . $titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List " . $titleModule,
            Router::url(['action' => 'view']) => $titlesubModule
        ];
        $this->set(compact('titleModule', 'breadCrumbs', 'titlesubModule'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $product = $this->Products->newEntity();
        if ($this->request->is('post')) {
            $product = $this->Products->patchEntity($product, $this->request->getData());
            if ($this->Products->save($product)) {
                $this->Flash->success(__('The product has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The product could not be saved. Please, try again.'));
        }
        $this->set(compact('product'));
        $titleModule = "Product";
        $titlesubModule = "Create " . $titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List " . $titleModule,
            Router::url(['action' => 'add']) => $titlesubModule
        ];
        $this->set(compact('titleModule', 'breadCrumbs', 'titlesubModule'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $product = $this->Products->get($id, [
            'contain' => []
        ]);

        if ($product->division == 'it') {
            $category = [
                'web' => 'Web',
                'mobile' => 'Mobile',
                'software' => 'Software',
                'hardware' => 'Hardware',
                'service' => 'Service'
            ];
        } else if ($product->division == 'website') {
            $category = [
                'company-profile' => 'Company Profile',
                'organization' => 'Organization',
                'personal-website' => 'Personal Website',
                'weeding' => 'Wedding'
            ];
        } else if ($product->division == 'medical-device') {
            $category = [
                'rumahsakit' => 'Medical Disposible',
                'survey' => 'Medical Equipment',
                'diagnostic' => 'Laboratorium Diagnostic',
                'telemedicine' => 'Telemedicine',
                'catalogue' => 'Catalogue'
            ];
        } else if ($product->division == 'catalog') {
            $category = [
                'medical-disposible' => 'Medical Disposible',
                'medical-equipment' => 'Medical Equipment',
                'interactive-flat' => 'Interactive Flat Panel Display'
            ];
        } else if ($product->division == 'consultant') {
            $category = [
                'consultant' => 'Consultant Management',
            ];
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $product = $this->Products->patchEntity($product, $this->request->getData());
            if ($this->Products->save($product)) {
                $this->Flash->success(__('The product has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The product could not be saved. Please, try again.'));
        }
        $this->set(compact('product'));
        $titleModule = "Product";
        $titlesubModule = "Edit " . $titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List " . $titleModule,
            Router::url(['action' => 'edit', $id]) => $titlesubModule
        ];
        $this->set(compact('titleModule', 'breadCrumbs', 'titlesubModule', 'category'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $product = $this->Products->get($id);
        if ($this->Products->delete($product)) {
            $code = 200;
            $message = __('The product has been deleted.');
            $status = 'success';
        } else {
            $code = 99;
            $message = __('The product could not be deleted. Please, try again.');
            $status = 'error';
        }
        if ($this->request->is('ajax')) {
            $this->set('code', $code);
            $this->set('message', $message);
            $this->set('_serialize', ['code', 'message']);
        } else {
            $this->Flash->{$status}($message);
            return $this->redirect(['action' => 'index']);
        }
    }
}
