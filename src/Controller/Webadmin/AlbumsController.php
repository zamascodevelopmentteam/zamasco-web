<?php
namespace App\Controller\Webadmin;

use App\Controller\AppController;
use cake\Routing\Router;
use Cake\Filesystem\Folder;
/**
 * Albums Controller
 *
 * @property \App\Model\Table\AlbumsTable $Albums
 *
 * @method \App\Model\Entity\Album[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AlbumsController extends AppController
{
  public function initialize()
  {
      parent::initialize();
      if(php_sapi_name() !== 'cli'){
          $this->Auth->allow(['index']);
      }

  }

  function beforeFilter(\Cake\Event\Event $event){
      parent::beforeFilter($event);

      $actions = [
          'add',
          'edit'
      ];

      if (in_array($this->request->params['action'], $actions)) {
          // for csrf
          $this->eventManager()->off($this->Csrf);

          // for security component
          $this->Security->config('unlockedActions', $actions);
      }

      if(isset($this->Security) && $this->request->isAjax() && ($this->action = 'index' || $this->action = 'delete')){

          $this->Security->config('validatePost',false);
          //$this->getEventManager()->off($this->Csrf);

      }

  }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
      if($this->request->is('ajax')){
          $source = $this->Albums;
          $searchAble = [
              'Albums.id',
              'Albums.title',
              'Albums.image'
          ];
          $data = [
              'source'=>$source,
              'searchAble' => $searchAble,
              'defaultField' => 'Albums.id',
              'defaultSort' => 'desc',
              'defaultSearch' => [
                  // [
                  //     'keyField' => 'group_id',
                  //     'condition' => '=',
                  //     'value' => 1
                  // ]
              ],
              // 'contain' => ['Groups']

          ];
          $asd   = $this->Datatables->make($data);
          //$this->set('data', $asd);
          $data = $asd['data'];
          $meta = $asd['meta'];
          $this->set('data',$data);
          $this->set('meta',$meta);
          $this->set('_serialize',['data','meta']);
      }else{
          $titleModule = "Album";
          $titlesubModule = "List Album";
          $breadCrumbs = [
              Router::url(['action' => 'index']) => $titlesubModule
          ];
          $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
      }
    }

    /**
     * View method
     *
     * @param string|null $id Album id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $album = $this->Albums->get($id, [
            'contain' => ['Pictures']
        ]);

        $this->set('album', $album);
        $titleModule = "Album";
        $titlesubModule = "View ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'view']) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $album = $this->Albums->newEntity();
        if ($this->request->is('post')) {
            $album = $this->Albums->patchEntity($album, $this->request->getData());

            // pr($this->request->data('image')); die;
            if ($this->Albums->save($album)) {
                $this->Flash->success(__('The album has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The album could not be saved. Please, try again.'));
        }
        $this->set(compact('album'));
        $titleModule = "Albums";
        $titlesubModule = "Create ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'add']) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Album id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $album = $this->Albums->get($id, [
            'contain' => []
        ]);


        if ($this->request->is(['patch', 'post', 'put'])) {
            $album = $this->Albums->patchEntity($album, $this->request->getData());
            if ($this->Albums->save($album)) {
                $this->Flash->success(__('The album has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The album could not be saved. Please, try again.'));
        }
        $this->set(compact('album'));
        $titleModule = "Album";
        $titlesubModule = "Edit ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'edit',$id]) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Album id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
      $this->request->allowMethod(['post', 'delete']);
      $album = $this->Albums->get($id);
      if ($this->Albums->delete($album)) {
          $code = 200;
          $message = __('The album has been deleted.');
          $status = 'success';

      } else {
          $code = 99;
          $message = __('The album could not be deleted. Please, try again.');
          $status = 'error';
      }
      if($this->request->is('ajax')){
          $this->set('code',$code);
          $this->set('message',$message);
          $this->set('_serialize',['code','message']);
      }else{
          $this->Flash->{$status}($message);
          return $this->redirect(['action' => 'index']);
      }
    }
}
