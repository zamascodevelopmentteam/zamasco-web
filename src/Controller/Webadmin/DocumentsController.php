<?php

namespace App\Controller\Webadmin;

use App\Controller\AppController;
use Cake\Routing\Router;

/**
 * Documents Controller
 *
 * @property \App\Model\Table\DocumentsTable $Documents
 *
 * @method \App\Model\Entity\Document[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DocumentsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        if (php_sapi_name() !== 'cli') {
            $this->Auth->allow(['index']);
        }
    }

    function beforeFilter(\Cake\Event\Event $event)
    {
        parent::beforeFilter($event);

        if (isset($this->Security) && $this->request->isAjax() && ($this->action = 'index' || $this->action = 'delete')) {

            $this->Security->config('validatePost', false);
            //$this->getEventManager()->off($this->Csrf);

        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if ($this->request->is('ajax')) {
            $source = $this->Documents;
            $searchAble = [
                'Documents.id',
                'Products.name',
                'Documents.file'
            ];
            $data = [
                'source' => $source,
                'searchAble' => $searchAble,
                'defaultField' => 'Products.id',
                'defaultSort' => 'desc',
                'defaultSearch' => [
                    // [
                    //     'keyField' => 'group_id',
                    //     'condition' => '=',
                    //     'value' => 1
                    // ]
                ],
                'contain' => ['Products']

            ];
            $asd   = $this->Datatables->make($data);
            //$this->set('data', $asd);
            $data = $asd['data'];
            $meta = $asd['meta'];
            $this->set('data', $data);
            $this->set('meta', $meta);
            $this->set('_serialize', ['data', 'meta']);
        } else {
            $titleModule = "Document";
            $titlesubModule = "List Document";
            $breadCrumbs = [
                Router::url(['action' => 'index']) => $titlesubModule
            ];
            $this->set(compact('titleModule', 'breadCrumbs', 'titlesubModule'));
        }
    }

    /**
     * View method
     *
     * @param string|null $id Document id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $document = $this->Documents->get($id, [
            'contain' => ['Products']
        ]);

        $this->set('document', $document);
        $titleModule = "Product";
        $titlesubModule = "View " . $titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List " . $titleModule,
            Router::url(['action' => 'view']) => $titlesubModule
        ];
        $this->set(compact('titleModule', 'breadCrumbs', 'titlesubModule'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $document = $this->Documents->newEntity();
        if ($this->request->is('post')) {
            $random = rand(1, 99);
            $data = $this->request->getData();
            $file_name = $data['file']['name'];
            $tmp_file = $data['file']['tmp_name'];
            $directory = "assets/file/";
            if (file_exists($directory . $file_name)) {
                $file_name = $random . "_" . $data['file']['name'];
                $tmp_file = $data['file']['tmp_name'];
            }
            if (file_exists($directory . $file_name)) {
                $this->Flash->error(__('The file already exists. Please, try again or rename the file.'));
            } else {
                if (!is_dir($directory)) mkdir($directory, 0777, true);
                if (move_uploaded_file($tmp_file, $directory . $file_name)) {
                    $data['file'] = $file_name;
                    $document = $this->Documents->patchEntity($document, $data);
                    if ($this->Documents->save($document)) {
                        $this->Flash->success(__('The document has been saved.'));

                        return $this->redirect(['action' => 'index']);
                    }
                    $this->Flash->error(__('The document could not be saved. Please, try again.'));
                }
                $this->Flash->error(__('The document could not be saved. Please, try again..'));
            }
        }
        $products = $this->Documents->Products->find('list', ['limit' => 200, 'order' => 'id DESC']);
        $this->set(compact('document', 'products'));
        $titleModule = "Document";
        $titlesubModule = "Create " . $titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List " . $titleModule,
            Router::url(['action' => 'add']) => $titlesubModule
        ];
        $this->set(compact('titleModule', 'breadCrumbs', 'titlesubModule'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Document id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $document = $this->Documents->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $random = rand(1, 99);
            $data = $this->request->getData();
            $file_name = $data['file']['name'];
            $tmp_file = $data['file']['tmp_name'];
            if (file_exists("assets/file/" . $file_name)) {
                $file_name = $random . "_" . $data['file']['name'];
                $tmp_file = $data['file']['tmp_name'];
            }
            if ($data['file']['name'] == null) {
                $data['file'] = $document->file;
                $document = $this->Documents->patchEntity($document, $data);
                if ($this->Documents->save($document)) {
                    $this->Flash->success(__('The document has been saved.'));

                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__('The document could not be saved. Please, try again.'));
            } else {
                $directory = "assets/file/";
                if (!unlink($directory . $document->file)) {
                    $this->Flash->error(__('The document could not be saved. Please, try again..'));
                } else {
                    if (file_exists($directory . $file_name)) {
                        $file_name = $random . "_" . $data['file']['name'];
                        $tmp_file = $data['file']['tmp_name'];
                    } else {
                        
                        if (!is_dir($directory)) mkdir($directory, 0777, true);
                        if (move_uploaded_file($tmp_file, $directory . $file_name)) {
                            $data['file'] = $file_name;
                            $document = $this->Documents->patchEntity($document, $data);
                            if ($this->Documents->save($document)) {
                                $this->Flash->success(__('The document has been saved.'));

                                return $this->redirect(['action' => 'index']);
                            }
                            $this->Flash->error(__('The document could not be saved. Please, try again.'));
                        } else {
                            $this->Flash->error(__('The document could not be saved. Please, try again.'));
                        }
                    }
                }
            }
        }
        $products = $this->Documents->Products->find('list', ['limit' => 200, 'order' => 'id DESC']);
        $this->set(compact('document', 'products'));
        $titleModule = "Document";
        $titlesubModule = "Edit " . $titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List " . $titleModule,
            Router::url(['action' => 'edit', $id]) => $titlesubModule
        ];
        $this->set(compact('titleModule', 'breadCrumbs', 'titlesubModule', 'category'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Document id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $data = $this->Documents->get($id);
        if (!unlink("assets/file/" . $data->file)) {
            $this->Flash->error(__('The document could not be deleted. Please, try again.'));
        } else {
            if ($this->Documents->delete($data)) {
                $code = 200;
                $message = __('The product has been deleted.');
                $status = 'success';
            } else {
                $code = 99;
                $message = __('The product could not be deleted. Please, try again.');
                $status = 'error';
            }
            if ($this->request->is('ajax')) {
                $this->set('code', $code);
                $this->set('message', $message);
                $this->set('_serialize', ['code', 'message']);
            } else {
                $this->Flash->{$status}($message);
                return $this->redirect(['action' => 'index']);
            }
        }
    }
}
