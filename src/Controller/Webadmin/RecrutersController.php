<?php
namespace App\Controller\Webadmin;

use App\Controller\AppController;
use Cake\Routing\Router;

/**
 * Recruters Controller
 *
 * @property \App\Model\Table\RecrutersTable $Recruters
 *
 * @method \App\Model\Entity\Recruter[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RecrutersController extends AppController
{
    function beforeFilter(\Cake\Event\Event $event){
        parent::beforeFilter($event);

        if(isset($this->Security) && $this->request->isAjax() && ($this->action = 'index' || $this->action = 'delete')){

            $this->Security->config('validatePost',false);
            //$this->getEventManager()->off($this->Csrf);

        }

    }

    public function index()
    {
        if($this->request->is('ajax')){
            $source = $this->Recruters;
            $searchAble = [
                'Recruters.id',
                'Recruters.job_title',
                'Recruters.name',
            ];
            $data = [
                'source'=>$source,
                'searchAble' => $searchAble,
                'defaultField' => 'Recruters.id',
                'defaultSort' => 'desc',
                'defaultSearch' => [
                    // [
                    //     'keyField' => 'group_id',
                    //     'condition' => '=',
                    //     'value' => 1
                    // ]
                ],
                // 'contain' => ['Groups']

            ];
            $asd   = $this->Datatables->make($data);
            //$this->set('data', $asd);
            $data = $asd['data'];
            $meta = $asd['meta'];
            $this->set('data',$data);
            $this->set('meta',$meta);
            $this->set('_serialize',['data','meta']);
        }else{
            $titleModule = "Recruters";
            $titlesubModule = "List Recruters";
            $breadCrumbs = [
                Router::url(['action' => 'index']) => $titlesubModule
            ];
            $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
        }
    }

    public function view($id = null)
    {
        $recruter = $this->Recruters->get($id, [
            'contain' => []
        ]);

        $this->set('recruter', $recruter);
        $titleModule = "Recruter";
        $titlesubModule = "View ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'view']) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }
}
