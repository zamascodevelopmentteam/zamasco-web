<?php

namespace App\Controller\Webadmin;

use App\Controller\AppController;
use cake\Routing\Router;

/**
 * Pictures Controller
 *
 * @property \App\Model\Table\PicturesTable $Pictures
 *
 * @method \App\Model\Entity\Picture[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PicturesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        if (php_sapi_name() !== 'cli') {
            $this->Auth->allow(['index']);
        }
    }

    function beforeFilter(\Cake\Event\Event $event)
    {
        parent::beforeFilter($event);

        if (isset($this->Security) && $this->request->isAjax() && ($this->action = 'index' || $this->action = 'delete')) {

            $this->Security->config('validatePost', false);
            //$this->getEventManager()->off($this->Csrf);

        }

        if (!$this->Auth->user()) {
            $this->redirect($this->Auth->logout());
        }
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $url = explode('=', Router::url($this->request->here(), true));
        $this->set(compact('url'));
        $contain = ucfirst($url[1] . 's');
        if ($this->request->is('ajax')) {
            $source = $this->Pictures;
            $searchAble = [
                'Pictures.id',
                'Pictures.name',
                'Pictures.path'
            ];
            $data = [
                'source' => $source,
                'searchAble' => $searchAble,
                'defaultField' => 'Pictures.id',
                'defaultSort' => 'desc',
                'defaultSearch' => [
                    // [
                    //     'keyField' => 'group_id',
                    //     'condition' => '=',
                    //     'value' => 1
                    // ]
                ],
                'contain' => [$contain]

            ];
            $asd   = $this->Datatables->make($data);
            //$this->set('data', $asd);
            $data = $asd['data'];
            $meta = $asd['meta'];
            $this->set('data', $data);
            $this->set('meta', $meta);
            $this->set('_serialize', ['data', 'meta']);
        } else {
            $titleModule = ucfirst($url[1]) . " Pictures";
            $titlesubModule = "List Content";
            $breadCrumbs = [
                Router::url(['action' => 'index']) => $titlesubModule
            ];
            $this->set(compact('titleModule', 'breadCrumbs', 'titlesubModule'));
        }
    }

    /**
     * View method
     *
     * @param string|null $id Picture id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $url = explode('=', Router::url($this->request->here(), true));

        $contain = ucfirst($url[1]) . 's';
        $picture = $this->Pictures->get($id, [
            'contain' => [$contain]
        ]);

        $this->set('picture', $picture);
        $titleModule = ucfirst($url[1]) . " Pictures";
        $titlesubModule = "View " . $titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List " . $titleModule,
            Router::url(['action' => 'view']) => $titlesubModule
        ];
        $this->set(compact('titleModule', 'breadCrumbs', 'titlesubModule', 'url'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $url = explode('=', Router::url($this->request->here(), true));

        $picture = $this->Pictures->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();

            $picture = $this->Pictures->newEntities($data);
            if ($this->Pictures->saveMany($picture)) {
                $this->Flash->success(__('The picture has been saved.'));

                return $this->redirect(['action' => 'index', '?' => ['id' => $url[1]]]);
            }
            $this->Flash->error(__('The picture could not be saved. Please, try again.'));
        }

        if ($url[1] == "album") {
            $pages = $this->Pictures->Albums->find('list', ['limit' => 200]);
        } else if ($url[1] == "product") {
            $pages = $this->Pictures->Products->find('list', ['limit' => 200, 'order' => 'id DESC']);
        }


        $this->set(compact('picture', 'pages', 'url'));
        $titleModule = ucfirst($url[1]) . " Pictures";
        $titleModule = "Pictures";
        $titlesubModule = "Create " . $titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List " . $titleModule,
            Router::url(['action' => 'add']) => $titlesubModule
        ];
        $this->set(compact('titleModule', 'breadCrumbs', 'titlesubModule'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Picture id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $url = explode('=', Router::url($this->request->here(), true));

        $picture = $this->Pictures->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $picture = $this->Pictures->patchEntity($picture, $this->request->getData());
            if ($this->Pictures->save($picture)) {
                $this->Flash->success(__('The picture has been saved.'));

                return $this->redirect(['action' => 'index', '?' => ['id' => $url[1]]]);
            }
            $this->Flash->error(__('The picture could not be saved. Please, try again.'));
        }

        if ($url[1] == "album") {
            $pages = $this->Pictures->Albums->find('list', ['limit' => 200]);
        } else if ($url[1] == "product") {
            $pages = $this->Pictures->Products->find('list', ['limit' => 200, 'order' => 'id DESC']);
        }

        $this->set(compact('picture', 'pages', 'url'));
        $titleModule = ucfirst($url[1]) . " Pictures";
        $titlesubModule = "Edit " . $titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List " . $titleModule,
            Router::url(['action' => 'edit', $id]) => $titlesubModule
        ];
        $this->set(compact('titleModule', 'breadCrumbs', 'titlesubModule'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Picture id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $url = explode('=', Router::url($this->request->here(), true));

        $this->request->allowMethod(['post', 'delete']);
        $picture = $this->Pictures->get($id);

        if ($this->Pictures->delete($picture)) {
            $code = 200;
            $message = __('The picture has been deleted.');
            $status = 'success';
            rmdir();
        } else {
            $code = 99;
            $message = __('The picture could not be deleted. Please, try again.');
            $status = 'error';
        }
        if ($this->request->is('ajax')) {
            $this->set('code', $code);
            $this->set('message', $message);
            $this->set('_serialize', ['code', 'message']);
        } else {
            $this->Flash->{$status}($message);
            return $this->redirect(['action' => 'index', '?' => ['id' => $url[1]]]);
        }
    }
}
