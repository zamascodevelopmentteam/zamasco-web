<?php
namespace App\Controller\Webadmin;

use App\Controller\AppController;
use Cake\Routing\Router;
use Cake\I18n\Time;
use Cake\Auth\DefaultPasswordHasher;

class PagesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        if(php_sapi_name() !== 'cli'){
            $this->Auth->allow(['index','users','groups','contents','uploadMedia']);
        }
    }

    function beforeFilter(\Cake\Event\Event $event){
        parent::beforeFilter($event);

        if(isset($this->Security) && $this->action = 'index' ){
            // $this->Security->config('unlockedFields',['groups']);
        }
    }


    public function index()
    {
        if(empty($this->Auth->user('id'))){
            $this->viewBuilder()->layout('login');
            if ($this->request->is('post')) {
                $this->loadModel('Users');
                $username = $this->request->getData('username');
                $password = $this->request->getData('password');
                $checkUser  = $this->Users->find('all',[
                    'contain' => [
                        'Aros','Groups'
                    ],
                    'conditions' => [
                        'username' => $username,
                        'Users.status' => 1
                    ]
                ])->first();
                $url = "";
                if(!empty($checkUser)){
                    $hasher = new DefaultPasswordHasher;
                    if($hasher->check($password,$checkUser->password)){
                        $code       = 200;
                        $message    = "Welcome ". $username;
                        $url = Router::url($this->Auth->redirectUrl(),true);
                    }else{
                        $code       = 50;
                        $message    = "Invalid password for ". $username. ", Please try again.";
                    }
                }else{
                    $code       = 50;
                    $message    = "Invalid username ". $username;
                }
                $user = $checkUser;
                if ($code == 200) {
                    $this->Auth->setUser($user);
                    $this->Redis->createCacheUserAuth($user);
                    if(!$this->request->is('ajax')){
                        return $this->redirect([
                            'controller'=> 'Pages',
                            'action' => 'index'
                        ]);
                    }
                } else {
                    if(!$this->request->is('ajax')){
                        $this->Flash->error(__('Username or password is incorrect'));
                        $this->render('login');
                    }
                }
                $this->set(compact('code','message','url','user'));
                $this->set('_serialize',['code','message','user','url']);
            }else{
               $this->render('login');
            }


        }else{
           $home_url = $this->Redis->readCacheUrlHome($this->userData->aro->id);
            return $this->redirect(['controller'=>'Dashboard']);
        }
    }
    public function logout()
    {
        $this->Redis->destroyCacheUrlHome($this->userData->aro->id);
        $this->Redis->destroyCacheSideNav($this->userData->aro->id);
        $this->Redis->destroyCacheUserAuth($this->userData);
        $this->redirect($this->Auth->logout());
    }

    public function editProfile()
    {
        $id = $this->userId;
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData(),[
                'validate'=>'editProfile'
            ]);
            if(empty($user->password)){
                unset($user->password);
            }
            if ($this->Users->save($user)) {
                $this->Redis->destroyCacheUserAuth($user);
                $this->Redis->createCacheUserAuth($user);
                $this->Flash->success(__('The profile has been updated.'));
                $this->autoRender = false;
                return $this->redirect(['action' => 'editProfile']);
            }
            $this->Flash->error(__('The profile could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
        $titleModule = "Profile";
        $titlesubModule = "Edit ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'editProfile']) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }

    public function activitiesLog()
    {
        $id = $this->userId;
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        $this->request->allowMethod(['get']);
        $this->loadModel('AuditLogs');
        $auditLogs = $this->AuditLogs->find('all',[
            'contain'=>[
                'Users' => [
                    'conditions' => [
                        'user_id' => $id
                    ]
                ]
            ],
            'order'=>['AuditLogs.timestamp' => 'DESC'],
            'conditions' => [

            ]

        ]);
        if($this->request->is('ajax')){
            $auditLogs->where('DATE(timestamp) >= DATE_SUB(NOW(), INTERVAL 10 DAY)');
        }
        $logs = [];
        foreach($auditLogs as $key => $auditLog){
            $time =new Time($auditLog->timestamp);
            $time = $time->timeAgoInWords(['accuracy' => 'day','end'=>'+7 day']);
            $logs[$key] = [
                'time' => $time,
                'description' => 'Has been '.$auditLog->type. ' '. $auditLog->source,
                'id' => $auditLog->id
            ];
        }
        $code = 200;
        $message = __('Get data activity logs');
        $status = 'success';
        $this->set('code',$code);
        $this->set('message',$message);
        $this->set('auditLogs',$logs);
        $titleModule = "Activities Logs";
        $titlesubModule = "List ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'activitiesLogs']) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
        if($this->request->is('ajax')){
            $this->set('_serialize',['code','message','auditLogs']);
        }
    }

    public function uploadMedia()
    {
        $data = $this->request->data['file'];
        $uploadFolder = WWW_ROOT.'assets'.DS.'img'.DS.'media/'.$data['name'];
        $saveDir = '/assets/img/media/'.$data['name'];
        $extension  = pathinfo($data['name'], PATHINFO_EXTENSION);
        move_uploaded_file($data['tmp_name'],$uploadFolder);
        $url = Router::url($saveDir,true);
        $this->set('url',$url);
        $this->set('_serialize',['url']);
    }

    public function productDetail(){

    }
}
