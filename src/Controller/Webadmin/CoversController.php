<?php
namespace App\Controller\Webadmin;

use App\Controller\AppController;
use Cake\Routing\Router;

use Cake\ORM\TableRegistry;
/**
 * Covers Controller
 *
 * @property \App\Model\Table\CoversTable $Covers
 *
 * @method \App\Model\Entity\Cover[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CoversController extends AppController
{
    public $animations = ['none','bounceIn','bounceInDown','bounceInLeft','bounceInRight','bounceInUp','fadeIn','fadeInDown','fadeInLeft','fadeInRight','fadeInUp','slideInUp','slideInDown','slideInLeft','slideInRight','zoomIn'];
    public function initialize()
    {
        parent::initialize();
        if(php_sapi_name() !== 'cli'){
            $this->Auth->allow(['index','add','delete','edit','view']);
        }

    }

    function beforeFilter(\Cake\Event\Event $event){
        parent::beforeFilter($event);

        if(isset($this->Security) && $this->request->isAjax() && ($this->action = 'index' || $this->action = 'delete')){

            $this->Security->config('validatePost',false);
            //$this->getEventManager()->off($this->Csrf);

        }

        if (!$this->Auth->user()) {
            $this->redirect($this->Auth->logout());
        }
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if($this->request->is('ajax')){
            $source = $this->Covers;
            $searchAble = [
                'Covers.id',
                'Covers.title',
                'Covers.pages',
                'Covers.picture',
                'Covers.status',
                'Covers.created'
            ];
            $data = [
                'source'=>$source,
                'searchAble' => $searchAble,
                'defaultField' => 'Covers.id',
                'defaultSort' => 'desc',
                'defaultSearch' => [
                    // [
                    //     'keyField' => 'group_id',
                    //     'condition' => '=',
                    //     'value' => 1
                    // ]
                ],
                // 'contain' => ['Groups']

            ];
            $asd   = $this->Datatables->make($data);
            //$this->set('data', $asd);
            $data = $asd['data'];
            $meta = $asd['meta'];
            $this->set('data',$data);
            $this->set('meta',$meta);
            $this->set('_serialize',['data','meta']);
        }else{
            $titleModule = "Cover";
            $titlesubModule = "List Cover";
            $breadCrumbs = [
                Router::url(['action' => 'index']) => $titlesubModule
            ];
            $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
        }
    }

    /**
     * View method
     *
     * @param string|null $id Cover id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $cover = $this->Covers->get($id, [
            'contain' => []
        ]);

        $this->set('cover', $cover);
        $titleModule = "Cover";
        $titlesubModule = "View ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'view']) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $cover = $this->Covers->newEntity();
        if ($this->request->is('post')) {
            $cover = $this->Covers->patchEntity($cover, $this->request->getData());
            if ($this->Covers->save($cover)) {
                $this->Flash->success(__('The cover has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The cover could not be saved. Please, try again.'));
        }

        // pages
        $pages = (['HOME','ABOUT','PRODUCT','PORTFOLIO','GALLERY', 'CAREER', 'CONTACT']);
        $pagesAll = array_combine($pages, $pages);
        $animations = array_combine($this->animations, $this->animations); 

        $this->set(compact('cover','pagesAll'));
        $titleModule = "Cover";
        $titlesubModule = "Create ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'add']) => $titlesubModule
        ];
        $this->set(compact('titleModule', 'breadCrumbs', 'titlesubModule','animations'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Cover id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $cover = $this->Covers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $cover = $this->Covers->patchEntity($cover, $data,['validate' => false]);
            if ($this->Covers->save($cover)) {
                $this->Flash->success(__('The cover has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The cover could not be saved. Please, try again.'));
        }
        $pages = (['HOME','ABOUT','PRODUCT','PORTFOLIO','GALLERY', 'CAREER', 'CONTACT']);
        $pagesAll = array_combine($pages, $pages);
        $animations = array_combine($this->animations, $this->animations);

        $this->set(compact('cover', 'pagesAll'));
        $titleModule = "Cover";
        $titlesubModule = "Edit ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'edit',$id]) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule','animations'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Cover id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $cover = $this->Covers->get($id);
        if ($this->Covers->delete($cover)) {
            $code = 200;
            $message = __('The user has been deleted.');
            $status = 'success';
        } else {
            $code = 99;
            $message = __('The user could not be deleted. Please, try again.');
            $status = 'error';
        }
        if($this->request->is('ajax')){
            $this->set('code',$code);
            $this->set('message',$message);
            $this->set('_serialize',['code','message']);
        }else{
            $this->Flash->{$status}($message);
            return $this->redirect(['action' => 'index']);
        }
    }
}
