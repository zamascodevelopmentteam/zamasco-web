<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;
use Cake\I18n\Time;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\TableRegistry;
use Cake\Network\Exception\NotFoundException;
use Cake\Mailer\Email;


class PagesController extends AppController
{
  public function initialize()
  {
    parent::initialize();
    $this->loadComponent('RequestHandler');
    if (php_sapi_name() !== 'cli') {
      $this->Auth->allow(['index', 'about', 'product', 'gallery', 'career', 'portfolio', 'contact', 'detail']);
    }
  }

  public function index()
  {
    $cover = TableRegistry::get('Covers');
    $content = TableRegistry::get('Contents');
    $product = TableRegistry::get('Products');
    $bannersRaw = $cover->find()->where("pages = 'HOME' AND status = '1'");

    $products = $content->find()->where("position ='products' AND status = '1'")->order('rand()')->first();
    $videos = $content->find()->where("page = 'index' AND position = 'video' AND status = '1'");
    # divisions
    $divisions = [];
    $consultant = $content->find()->where("page = 'index' AND position = 'consultant' AND status = '1'")->first();
    $medicalDevice = $content->find()->where("page = 'index' AND position = 'medical-device' AND status = '1'")->first();
    $technology = $content->find()->where("page = 'index' AND position = 'technology' AND status = '1'")->first();
    if (!empty($consultant)) $divisions[] = $consultant;
    if (!empty($medicalDevice)) $divisions[] = $medicalDevice;
    if (!empty($technology)) $divisions[] = $technology;
    # end divisions
    $judul = $content->find()->where("page = 'index' AND position = 'slogan' AND status = '1'");
    $footer = $content->find()->where("page = 'index' AND position = 'footer1' AND status = '1'");
    $products = $product->find()->where("status = '1'");

    $b = [];
    $idx = 0;
    foreach ($footer as $key => $value) {
      if ($key % 10 == 0) {
        $idx++;
      }
      $b[$idx][] = $value;
    }

    $no = 1;
    $video_cover = [];
    $banners = [];
    foreach ($bannersRaw as $key => $value) {
      if (pathinfo($value->picture, PATHINFO_EXTENSION) == 'mp4') $video_cover[] = $value;
      else $banners[] = $value;
    }
    $this->set(compact('banners', 'products', 'videos', 'divisions', 'judul', 'footer', 'no', 'b', 'video_cover'));
  }

  public function about()
  {
    $cover = TableRegistry::get('Covers');
    $content = TableRegistry::get('Contents');

    $banners = $cover->find()->where("pages ='ABOUT' OR pages = 'ALL' AND status = '1'");
    $this->set(compact('banners'));

    $logoVisiMisi = $content->find()->where("page = 'tentang' AND position = 'logo-visi-misi' AND status = '1'")->first();
    $visi = $content->find()->where("page = 'tentang' AND position = 'visi' AND status = '1'");
    $misi = $content->find()->where("page = 'tentang' AND position = 'misi' AND status = '1'");
    $tech = $content->find()->where("page = 'tentang' AND position = 'technology' AND status = '1'");
    $ehealth = $content->find()->where("page = 'tentang' AND position = 'ehealth' AND status = '1'");
    $consultant = $content->find()->where("page = 'tentang' AND position = 'consultant' AND status = '1'");
    $moto = $content->find()->where("page = 'tentang' AND position = 'moto' AND status = '1'");
    $moto1 = $content->find()->where("page = 'tentang' AND position = 'moto1' AND status = '1'");
    $anggota = $content->find()->where("page = 'tentang' AND position = 'anggota' AND status = '1'");
    $foteam = $content->find()->where("page = 'tentang' AND position = 'foteam' AND status = '1'")->first();
    $struktur = $content->find()->where("page = 'tentang' AND position = 'struktur' AND status = '1'");

    $b = [];
    $idx = 0;
    foreach ($struktur as $key => $value) {
      if ($key % 4 == 0) {
        $idx++;
      }
      $b[$idx][] = $value;
    }


    $this->set(compact('logoVisiMisi', 'visi', 'misi', 'tech', 'ehealth', 'consultant', 'moto', 'moto1', 'komisaris', 'anggota', 'foteam', 'struktur', 'b'));
  }

  public function product($id = null)
  {
    $cover = TableRegistry::get('Covers');
    $product = TableRegistry::get('Products');

    $banners = $cover->find()->where("pages ='PRODUCT' OR pages = 'ALL' AND status = '1'");
    $products = $product->find()->where("status = '1'");

    $this->set(compact('banners', 'products'));
  }

  public function portfolio()
  {
    $cover = TableRegistry::get('Covers');
    $product = TableRegistry::get('Products');

    $banners = $cover->find()->where("pages ='PORTFOLIO' OR pages = 'ALL' AND status = '1'");
    $products = $product->find()->where("status = '1' AND category != 'hardware'");

    $no = 1;
    $this->set(compact('banners', 'products', 'no'));
  }

  public function gallery()
  {
    $cover = TableRegistry::get('Covers');
    $album = TableRegistry::get('Albums');

    $banners = $cover->find()->where("pages ='GALLERY' OR pages = 'ALL' AND status = '1'");
    $albums = $album->find();

    $this->set(compact('banners', 'akhir', 'albums'));
  }

  public function career()
  {
    $cover = TableRegistry::get('Covers');
    $content = TableRegistry::get('Contents');
    $this->Recruters = TableRegistry::get('Recruters');

    $recruter = $this->Recruters->newEntity();
    if ($this->request->is('post')) {
      $recruter = $this->Recruters->patchEntity($recruter, $this->request->getData());
      if ($this->Recruters->save($recruter)) {
        $this->Flash->success(__('The recruter has been saved.'));

        return $this->redirect(['action' => 'career']);
      }
      $this->Flash->error(__('The recruter could not be saved. Please, try again.'));
    }
    $this->set(compact('recruter'));

    $banners = $cover->find()->where("pages = 'CAREER' OR pages = 'ALL' AND status = '1'");
    $cont = $content->find()->where("page = 'career' AND position = 'career' AND status = '1'");

    $this->set(compact('banners', 'cont'));
  }


  public function contact()
  {
    $cover = TableRegistry::get('Covers');

    $banners = $cover->find()->where("pages ='CONTACT' OR pages = 'ALL' AND status = '1'");

    $this->set(compact('banners'));
    $contact = TableRegistry::get('Contents');

    $video = $contact->find()->where("page = 'kontak' AND position = 'video' AND status = '1'")->first();
    $alamat = $contact->find()->where("page = 'kontak' AND position = 'alamat' AND status = '1'");
    $no_kan = $contact->find()->where("page = 'kontak' AND position = 'tlpn1' AND status = '1'");
    $no_sel = $contact->find()->where("page = 'kontak' AND position = 'tlpn2' AND status = '1'");
    $email = $contact->find()->where("page = 'kontak' AND position = 'email' AND status = '1'");
    $cont = $contact->find()->where("page = 'kontak' AND position = 'intro' AND status = '1'");

    // --------------------
    if ($this->request->is('post')) {
      $name = $this->request->data('name');
      $email = $this->request->data('email');
      $subjek = $this->request->data('subjek');
      $pesan = $this->request->data('pesan');

      Email::configTransport('gmail', [
        'host' => 'ssl://smtp.gmail.com',
        'port' => 465,
        'username' => "zamasco.web@gmail.com",
        'password' => "ZamascoWeb212.",
        'className' => 'Smtp'
      ]);

      $emails = new Email();
      $emails->transport('gmail');
      $emails->setFrom("zamasco.web@gmail.com", $name)
        ->setTo('reza@zamasco.co.id')
        ->subject($subjek)
        ->send("From : " . $email . "
" . $pesan);


      // pr($email); die;

    }


    $this->set(compact('video', 'alamat', 'no_kan', 'no_sel', 'email', 'cont'));
  }

  public function detail()
  {
    $cover = TableRegistry::get('Covers');
    $product = TableRegistry::get('Products');
    $picture = TableRegistry::get('Pictures');
    $album = TableRegistry::get('Albums');
    $document = TableRegistry::get('Documents');

    $url = explode('=', Router::url($this->request->here(), true));
    if (empty($url[1])) {
      $this->Flash->error(__('404'));
      $this->redirect(['action' => 'product']);
    } else {
      $preid = explode('&', $url[1]);
      $pages = $url[2];

      if ($pages == "product") {
        $product_id = $preid[0];
        $details = $product->find()->where("status = '1' AND id = '$product_id'");
        $logo1 = $product->find()->where("status = '1' AND id = '$product_id'");
        $kecil = $picture->find()->where("product_id = '$_GET[id]'");
        $logo2 = $picture->find()->where("product_id = '$_GET[id]'");
        $banners = $cover->find()->where("pages = 'PRODUCT' OR pages = 'ALL' AND status = '1'");
        $file = $document->find()->where("product_id = '$_GET[id]' AND status = '1'");
        $noan = $document->find()->where("product_id = '$_GET[id]' AND status = '1'")->first();
      } else if ($pages == "gallery") {
        $gallery_id = $preid[0];
        $details = $picture->find()->where("album_id = '$gallery_id'");
        $banners = $cover->find()->where("pages ='GALLERY' OR pages = 'ALL' AND status = '1'");
        $albums = $album->find()->where("id = '$gallery_id'");
      } else {
        $this->viewBuilder()->setLayout('error404');
      }
    }

    $this->set(compact('banners', 'details', 'pages', 'albums', 'kecil', 'logo1', 'logo2', 'file', 'noan'));
  }
}
