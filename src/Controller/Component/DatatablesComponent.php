<?php
// src/Controller/Component/DatatablesComponent.php
namespace App\Controller\Component;

use Cake\Controller\Component;

class DatatablesComponent extends Component
{

	public function make($data)
	{
        $conf = [];
		$source  = $data['source'];
		$allData = $data['source'];
		$searchAble = $data['searchAble'];
		$defaultSort = ! empty( $data[ 'defaultSort' ]) ? $data[ 'defaultSort' ] : 'ASC';
		$defaultField = ! empty( $data[ 'defaultField' ]) ? $data[ 'defaultField' ] : '';
        $defaultSearch = ! empty( $data[ 'defaultSearch' ]) ? $data[ 'defaultSearch' ] : '';
        if(!empty($data['contain'])){
            $conf['contain'] = $data['contain'];
        }
		if(! empty($defaultSearch))
		{
            $defaultWhere = [];
			foreach ($defaultSearch as $key => $condition) {
                $defaultWhere[] = [
                    $condition['keyField'].' '.$condition['condition'] => $condition['value']
                ];
            }
            if(!empty($defaultWhere))
            {
                $conf['conditions'] = $defaultWhere;
            }
		}
		$request = $this->request->data;
		$datatable = ! empty( $request ) ? $request : array();
		// pr($request);;
		$datatable = array_merge( array( 'pagination' => array(), 'sort' => array(), 'query' => array() ), $datatable );
		

		$filter = isset( $datatable[ 'query' ][ 'm_form_search' ] ) && is_string( $datatable[ 'query' ][ 'm_form_search' ] ) ? $datatable[ 'query' ][ 'm_form_search' ] : '';
		if ( ! empty( $filter ) ) {
		//	dd($filter);
            $orWhere = []; 
            foreach ($searchAble as $key => $value) {
                $orWhere[] = [$value.' LIKE' => '%'.$filter.'%'];
            }
            if(!empty($orWhere)){
                if(!empty($conf['conditions'])){
                    $conf['conditions'] = $conf['conditions'] + ['OR' => $orWhere];
                }else{
                    $conf['conditions'] =  ['OR' => $orWhere];
                }
                
            }
			unset( $datatable[ 'query' ][ 'generalSearch' ] );
		}

		$sort  = ! empty( $datatable[ 'sort' ][ 'sort' ] ) ? $datatable[ 'sort' ][ 'sort' ] : $defaultSort;
		$field = ! empty( $datatable[ 'sort' ][ 'field' ] ) && $datatable['sort']['field'] != 'actions' ? $datatable[ 'sort' ][ 'field' ] : $defaultField;
        
		if(!empty($field))
		{
            $conf['order'] =[$field => $sort];
		}

		$meta    = array();
		$page    = ! empty( $datatable[ 'pagination' ][ 'page' ] ) ? (int)$datatable[ 'pagination' ][ 'page' ] : 1;
		$perpage = ! empty( $datatable[ 'pagination' ][ 'perpage' ] ) ? (int)$datatable[ 'pagination' ][ 'perpage' ] : -1;

		$pages = 1;
		if(isset($data['union'])){
			$total = 0;
		}else{
			$total 	 = $source->find('all',$conf)->count();
		}
		// $perpage 0; get all data
		if ( $perpage > 0 ) {
			$pages  = ceil( $total / $perpage ); // calculate total pages
			$page   = max( $page, 1 ); // get 1 page when $_REQUEST['page'] <= 0
			$page   = min( $page, $pages ); // get last page when $_REQUEST['page'] > $totalPages
			$offset = ( $page - 1 ) * $perpage;
			if ( $offset < 0 ) {
				$offset = 0;
			}

            // $data = array_slice( $data, $offset, $perpage, true );
            $conf['limit'] = $perpage;
            $conf['offset']= $offset;
		}

		//dd($datatable);
		// $request = $this->request->all();
		// dd($request);		
		$meta = array(
			'page'    => $page,
			'pages'   => $pages,
			'perpage' => $perpage,
			'total'   => $total,
		);
		//pr($conf);
		$result = array(
			'meta' => $meta + array(
					'sort'  => $sort,
					'field' => $field,
					'sql' => $source->find('all',$conf)->sql()
				),
			'data' => $source->find('all',$conf),
        );
        
		return $result;

	}

}

?>