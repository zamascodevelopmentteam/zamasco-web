<h1>test 123213123</h1>

<!-- index banner -->

	<div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel" data-interval="5000" style=" height: 100%">

	  <div class="carousel-inner">


	    <?php foreach($banners as $banner): ?>
			<div class="item kenburn" id="trig" style="background-image: url(<?= substr($banner->picture_dir, 7).$banner->picture; ?>);background-size: cover; background-position: center;transform: scale(1.2);">
				<div style="height: 700px">
					<div class="container-fluid">
						<div class="col-md-8 col-md-offset-2" style="
							<?php if($no % 2 == 0): ?>
								text-align: center;
							<?php elseif ($no % 3 == 0): ?>
								text-align: right;
							<?php else: ?>
								text-align: left;
							<?php endif; ?>
						">

							<div id="title-content">
								<h1 class="animated
								<?php if($no % 2 == 0): ?>
									zoomIn
								<?php elseif ($no % 3 == 0): ?>
									fadeInRight
								<?php else: ?>
									fadeInLeft
								<?php endif; ?>
								 animation-delay-100" style="transform: scale(1);"><?= $banner->title ?></h1>
								<div class="lead animated
								<?php if($no % 2 == 0): ?>
									zoomIn
								<?php elseif ($no % 3 == 0): ?>
									fadeInRight
								<?php else: ?>
									fadeInLeft
								<?php endif; ?>
								 animation-delay-200"><?= $banner->text_content ?></div>
							</div>
						</div>
					</div>
				</div>
		    </div>
		    <?php $no++ ?>
	    <?php endforeach; ?>

		<script>
			$(document).ready(function(){
				$('#trig').addClass('active');
			});
		</script>


	</div>
</div>

<!-- end index banner -->

<!-- Transformer covers -->

'transformer' =>  function ($table, $entity, $data, $field, $settings) {
    $extension = pathinfo($data['name'], PATHINFO_EXTENSION);

    // Store the thumbnail in a temporary file
    $tmp = tempnam(sys_get_temp_dir(), 'upload') . '.' . $extension;

    // Use the Imagine library to DO THE THING
    $size = new \Imagine\Image\Box(100, 100);
    $mode = \Imagine\Image\ImageInterface::THUMBNAIL_INSET;
    $imagine = new \Imagine\Gd\Imagine();

    // Save that modified file to our temp file
    $imagine->open($data['tmp_name'])
        ->thumbnail($size, $mode)
        ->save($tmp);

    // Now return the original *and* the thumbnail
    return [
        $data['tmp_name'] => $data['name'],
        $tmp => 'thumbnail-' . $data['name'],
    ];
},

<!-- endTransformer -->

<!-- index covers -->

	
            },{
            field: "Covers.picture_dir",
            title: "",
            sort : 'asc',
            template: function(t) {
                return "<img src='" + t.picture_dir.substring(7) + t.picture + "' width='100' >"
            }

<!-- end index covers -->

<!-- vide background video -->

	<div style="width: 100%; height: 660px;"
	  data-vide-bg="<?= substr($banners->picture_dir, 7).$banners->picture; ?>">
	  <div style="padding-top: 15%;">
	  	<div class="container-fluid">
	  		<div class="col-md-8 col-md-offset-2" style="text-align: center;">
	  			<div>
	  				<h1 class="animated zoomIn animation-delay-100" style="transform: scale(1); font-size: 50px;">
		  				<?= $banners->title; ?>
		  			</h1>
	  				<div class="lead animated zoomIn animation-delay-200">
	  					<?= $banners->text_content; ?>
	  				</div>
	  			</div>
	  		</div>
	  	</div>
	  </div>
	</div>

<!-- end vide background video -->