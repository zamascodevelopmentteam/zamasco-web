	<div class="owl-carousel owl-theme" id="banner">
		<?php foreach ($banners as $banner) : ?>
			<div class="page-title" style="background-image: url(<?= substr($banner->picture_dir, 7) . $banner->picture ?>); background-position: center; height: 350px;">
			</div>
		<?php endforeach; ?>
	</div>


	<div class="section-block" style="background-color: rgb(242,242,242); margin-bottom: -30px;">

		<!-- Begin Division -->

		<div class="container">
			<center>
				<h3>- OUR SOLUTION -</h3>
			</center><br>
			<div class="filters">
				<div class="row">
					<div class="col-md-12">
						<div class="row">
							<div class="col-xs-6 col-sm-2">
								<center><a id="division1" href="#!" class="button btn-custom-black btn-custom-active" data-filter="">All</a></center>
							</div>
							<!-- <div class="col-xs-6 col-sm-2">
					      		<center><a id="division2" href="#ehealth" class="button btn-custom-black" data-filter=".ehealth">e-health</a></center>
					      	</div> -->
							<div class="col-xs-6 col-sm-2">
								<center><a id="division4" href="#medical-device" class="button btn-custom-black" data-filter=".medical-device">medical-device</a></center>
							</div>
							<div class="col-xs-6 col-sm-2">
								<center><a id="division3" href="#it" class="button btn-custom-black" data-filter=".it">technology</a></center>
							</div>
							<div class="col-xs-6 col-sm-2">
								<center><a id="division2" href="#website" class="button btn-custom-black" data-filter=".website">website</a></center>
							</div>
							<div class="col-xs-6 col-sm-2">
								<center><a id="division5" href="#catalog" class="button btn-custom-black" data-filter=".catalog">e-catalog</a></center>
							</div>
							<div class="col-xs-6 col-sm-2">
								<center><a id="division6" href="#consultant" class="button btn-custom-black" data-filter=".consultant">consultant management</a></center>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="section-block">
		<div class="container">
			<!-- End Division -->
			<div class="bungkus">
				<!-- Begin Category -->

				<div class="grid-category">

					<div class="col-md-10 col-md-offset-1 category-item it" style="display: none">
						<div class="ui-group">
							<!-- <fieldset> -->
							<!-- <legend>Category</legend> -->
							<div class="row justify-content-center">
								<!-- <div class="col-md-2 col-md-push-1 col-xs-6 ">
										<center> <button class="button btn-custom-black" data-filter=".web">website</button></center>
									</div> -->
								<div class="col-md-2 col-md-push-2 col-xs-6 ">
									<center><button class="button btn-custom-black" data-filter=".mobile">mobile</button></center>
								</div>
								<div class="col-md-2 col-md-push-2 col-xs-6 ">
									<center><button class="button btn-custom-black" data-filter=".software">software</button></center>
								</div>
								<div class="col-md-2 col-md-push-2 col-xs-6 ">
									<center><button class="button btn-custom-black" data-filter=".hardware">hardware</button></center>
								</div>
								<div class="col-md-2 col-md-push-2 col-xs-6 ">
									<center><button class="button btn-custom-black" data-filter=".service">services</button></center>
								</div>
							</div>

							<!-- </fieldset> -->
						</div>

					</div>


					<div class="col-md-8 col-md-offset-2 category-item website" style="display: none">

						<div class="ui-group">
							<!-- <fieldset> -->
							<!-- <legend>Category</legend> -->
							<div class="row justify-content-center">
								<div class="col-md-3 col-xs-6 ">
									<center> <button class="button btn-custom-black" data-filter=".company-profile">Company Profile</button></center>
								</div>
								<div class="col-md-3 col-xs-6 ">
									<center> <button class="button btn-custom-black" data-filter=".organization">Organization</button></center>
								</div>
								<div class="col-md-3 col-xs-6 ">
									<center> <button class="button btn-custom-black" data-filter=".personal-website">Personal Website</button></center>
								</div>
								<div class="col-md-3 col-xs-6 ">
									<center> <button class="button btn-custom-black" data-filter=".wedding">Wedding</button></center>
								</div>
							</div>

							<!-- </fieldset> -->
						</div>

					</div>


					<div class="col-md-10 category-item medical-device" style="display: none">

						<div class="ui-group">
							<!-- <fieldset> -->
							<!-- <legend>Category</legend> -->
							<div class="row justify-content-center">
								<div class="col-md-2 col-xs-6 col-md-push-2">
									<center> <button class="button btn-custom-black" data-filter=".rumahsakit">medical disposible</button></center>
								</div>
								<div class="col-md-2 col-xs-6 col-md-push-2">
									<center><button class="button btn-custom-black" data-filter=".survey">medical equipment</button></center>
								</div>
								<div class="col-md-2 col-xs-6 col-md-push-2">
									<center><button class="button btn-custom-black" data-filter=".diagnostic">laboratorium Diagnostic</button></center>
								</div>
								<div class="col-md-2 col-xs-6 col-md-push-2">
									<center><button class="button btn-custom-black" data-filter=".telemedicine">telemedicine</button></center>
								</div>
								<div class="col-md-2 col-xs-6 col-md-push-2">
									<center><button class="button btn-custom-black" data-filter=".catalogue">catalogue</button></center>
								</div>
							</div>

							<!-- </fieldset> -->
						</div>

					</div>


					<div class="col-md-8 col-md-offset-2 category-item catalog" style="display: none">

						<div class="ui-group">
							<!-- <fieldset> -->
							<!-- <legend>Category</legend> -->
							<div class="row justify-content-center">
								<div class="col-md-4 col-xs-6 ">
									<center> <button class="button btn-custom-black" data-filter=".medical-disposible">Medical Disposible</button></center>
								</div>
								<div class="col-md-4 col-xs-6 ">
									<center> <button class="button btn-custom-black" data-filter=".medical-equipment">Medical Equipment</button></center>
								</div>
								<div class="col-md-4 col-xs-6 ">
									<center> <button class="button btn-custom-black" data-filter=".interactive-flat">Interactive Flat Panel Display</button></center>
								</div>
							</div>

							<!-- </fieldset> -->
						</div>

					</div>
				</div>

				<!-- End Category -->
				<br>
				<!-- Begin Product Content -->

				<div class="grid-product" style="transition: 1s;">
					<div>
						<?php foreach ($products as $product) : ?>

							<div style="margin-bottom: 50px;" class="col-md-4 col-sm-6 col-xs-12 product-item all <?= $product->division ?> <?= $product->category ?> ">
								<div class="thumbnail">

									<img style="height: 250px;" src="<?= substr($product->picture_dir, 7) . $product->picture ?>" alt="blog-img" id="<?= $product->id ?>">
									<div class="caption" id="<?= $product->id ?>">
										<h4 style="text-align: center;"><?= $product->name ?></h4>
										<br><br>
										<div class="footer">
											<a href="detail?id=<?= $product->id ?>&pages=product" class="btn btn-primary pull-right ">
												Lihat Detail
											</a>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach ?>
						<!-- <div class="col-md-4 col-sm-6 col-xs-12 product-item IT WEB ">
						    <div class="thumbnail">

						        <img src="assets/img/content/projects/award.jpg" alt="blog-img" >
						        <div class="caption">

						        	<h4>Award Website</h4>
									<p style="margin-top: 10px;">
										Dalam membangun sebuah website dengan tujuan pengelola membari penghargaan tertentu terkait perorangan atau organisasi, maka titik berat pada website ini memberikan informasi yang jelas kepada pendaftar, dengan mengedepankan kemudahan navigasi, serta menu menu yang mudah di akses dan mudah dipahami.
									</p>
									<div class="footer">
										<a href="products/award-website.html" class="btn btn-primary pull-right ">
											View More
										</a>
									</div>

									<br>
						        </div>

						    </div>
						 </div> -->





					</div>


				</div>

				<!-- End Product Content -->
			</div>


		</div>

	</div>

	<?php $this->start('script') ?>

	<script>
		$('#banner').owlCarousel({
			items: 1,
			smartSpeed: 450,
			loop: true,
			autoplay: true,
			autoplayTimeout: 4000,
			autoplayHoverPause: true,
			lazyLoad: true,
			dots: false,
			autoHeight: true
			// autoWidth:true,
		});


		$('.grid-category').isotope({
			filter: '.all'
		});


		function hashCheck() {
			$('.filters a').removeClass('btn-custom-active');

			var hash = location.hash.substr(1);
			if (hash == "website") {
				$('.grid-category').isotope({
					filter: '.website'
				});
				$('.grid-product').isotope({
					itemSelector: '.product-item',
					filter: '.company-profile, .organization, .personal-website'
				});

				$('#division2').addClass('btn-custom-active');

			} else if (hash == "it") {
				$('.grid-category').isotope({
					filter: '.it'
				});
				$('.grid-product').isotope({
					itemSelector: '.product-item',
					filter: '.web, .mobile, .software, .hardware, .service'
				});

				$('#division3').addClass('btn-custom-active');

			} else if (hash == "medical-device") {
				$('.grid-category').isotope({
					filter: '.medical-device'
				});
				$('.grid-product').isotope({
					itemSelector: '.product-item',
					filter: '.rumahsakit, .survey, .diagnostic, .telemedicine, .catalogue'
				});

				$('#division4').addClass('btn-custom-active');
			} else if (hash == "consultant") {
				$('.grid-category').isotope({
					filter: '.all' // empty
				});
				$('.grid-product').isotope({
					itemSelector: '.product-item',
					filter: '.consultant'
				});

				$('#division6').addClass('btn-custom-active');
			} else if (hash == "catalog") {
				$('.grid-category').isotope({
					filter: '.catalog'
				});
				$('.grid-product').isotope({
					itemSelector: '.product-item',
					filter: '.catalog'
				});

				$('#division5').addClass('btn-custom-active');
			} else { //  all
				$('.grid-category').isotope({
					filter: '.all' // empty
				});
				$('.grid-product').isotope({
					itemSelector: '.product-item',
					filter: '.all'
				});

				$('#division1').addClass('btn-custom-active');
				// $('.grid-product').animate({top: '0%'});
			}

			$('.ui-group button').removeClass('btn-custom-active');
			$('.all').addClass('btn-custom-active');
		}
		hashCheck()
		$(window).on('hashchange', function(e) {
			hashCheck()
		});

		$('.grid-category').isotope({
			itemSelector: '.category-item',
			layoutMode: 'fitRows'

		})

		$('.filters a').click(function() {
			hashCheck()
		});
		
		$('.ui-group button').click(function() {
			$('.ui-group button').removeClass('btn-custom-active-2');
			$(this).addClass('btn-custom-active-2');

			var selectorProduct = $(this).attr('data-filter');
			$('.grid-product').isotope({
				itemSelector: '.product-item',
				filter: selectorProduct
			});
		});
	</script>

	<?php $this->end() ?>