<div class="owl-carousel owl-theme" id="banner">
<?php foreach($banners as $banner): ?>
	<div class="page-title " style="background-image: url(<?= substr($banner->picture_dir,7).$banner->picture ?>); background-position: center; height: 350px;">

	</div>
<?php endforeach; ?>
</div>

	<div class="section-block">
		<div class="container">

		<?php foreach($products as $product): ?>

			<div class="row" style="margin-top: 5%;">
				<div class="col-md-12">
					<div class="col-md-6
					<?php if($no % 2 == 0): ?>

					<?php else : ?>
						col-md-push-6
					<?php endif ?>
					" data-aos="fade-right">
						<img src="<?= substr($product->picture_dir, 7).$product->picture ?>" width="100%">
					</div>
					<div class="col-md-6
					<?php if($no % 2 == 0): ?>

					<?php else : ?>
						col-md-pull-6
					<?php endif ?>
					" data-aos="fade-left">
						<div style="margin-bottom: 35px;" class="section-heading-left">
							<h2> <?= $product->name ?></h2>
						</div>
						<p class="lead">
							<?= $product->content ?>
						</p>
					</div>
				</div>
			</div>
			<?php $no++ ?>
			<?php endforeach ?>

		</div>
	</div>

	<?php $this->start('script') ?>

	<script>
		AOS.init();
	</script>

	<?php $this->end() ?>
