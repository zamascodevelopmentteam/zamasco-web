<style>
	@media screen and (max-width: 768px) {
		.project-first {
			margin-top: 150px;
		}

		.carousel-height {
			height: 50%;
		}

		.overflow-scroll {
			overflow: scroll;
		}
	}

	@media screen and (min-width: 768px) {
		.carousel-height {
			height: 100%;
		}
	}

	.carousel-indicators li {
		border: 1px solid black;
	}

	.carousel-indicators .active {
		background: red;
	}

	.bg-heading-center {
		position: relative;
	}

	.bg-heading-center .text-i {
		margin: 0;
		position: absolute;
		top: 50%;
		left: 50%;
		-ms-transform: translate(-50%, -50%);
		transform: translate(-50%, -50%);
	}

	@media screen and (max-width: 991px) {
		.embed-responsive-video {
			margin-top: 63px;
		}
	}
</style>
<link href="/assets/video_js/video-js.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css" />
<?php if (count($video_cover)): ?>
	<div class="embed-responsive embed-responsive-video" id="scroll" style="padding-bottom: 28%;">
		<video id='video' class='video-js embed-responsive-item' preload='auto' autoplay data-setup='{}' muted>
			<p class='vjs-no-js'>
				To view this video please enable JavaScript, and consider upgrading to a web browser that
				<a href='https://videojs.com/html5-video-support/' target='_blank'>supports HTML5 video</a>
			</p>
		</video>
		<?php if (!empty($video_cover[0]->title)): ?>
			<div class="bg-heading-center" style="width: 100%;height: 500px;background: rgba(0,0,0,.4);position: absolute;top: 0;bottom: 0;left: 0;right: 0;z-index: 99;" id="scroll-overlay">
				<div class="text-i text-center">
					<h2 class="text-white" style="font-size: 2.5vw;"><?= strip_tags($video_cover[0]->title) ?></h2>
					<h4 class="text-white" style="font-size: 1.1vw;"><?= strip_tags($video_cover[0]->text_content) ?></h4>
				</div>
			</div>
		<?php endif; ?>
	</div>
<?php else: ?>
	<style>
		.slide-wrapper .slide-text {
			transition: 1s;
			opacity: 0;
		}

		.carousel-inner .item.active .slide-wrapper .slide-text {
			transition: 1s;
			opacity: 1;
		}
	</style>
	<div id="myCarousel" class="carousel slide carousel-fade carousel-height" data-ride="carousel" data-interval="5000">
		<style>
			@media only screen and (max-width: 1000px) {
				.carousel-indicators {
					top: 75%;
				}
			}
		</style>
		<ol class="carousel-indicators" style="text-align: right;float: right;right: 2% !important;left: inherit;">
			<?php foreach ($banners as $i => $bqwe): ?>
				<li data-target="#myCarousel" data-slide-to="<?= $i ?>" class="<?= ($i == 0 ? 'active' : '') ?>"></li>
			<?php endforeach; ?>
		</ol>
		<div class="carousel-inner">
			<?php foreach ($banners as $key => $banner): ?>
				<div class="item <?= $key == 0 ? 'active' : '' ?>">
					<img src="<?= substr($banner->picture_dir, 7) . $banner->picture; ?>" width="100%" style="height: 100%;">
					<?php if (!empty($banner->title)): ?>
						<div class="bg-heading-center" style="width: 100%;height: 100vh;background: rgba(0,0,0,.4);position: absolute;top: 0;bottom: 0;left: 0;right: 0;z-index: 99;" id="scroll-overlay">
							<div class="text-i text-center">
								<div class="slide-wrapper">
									<h2 class="text-white slide-text" style="font-size: 2.5vw;"><?= strip_tags($banner->title) ?></h2>
									<h4 class="text-white slide-text" style="font-size: 1.1vw;"><?= strip_tags($banner->text_content) ?></h4>
								</div>
							</div>
						</div>
					<?php endif; ?>
				</div>
				<?php $no++ ?>
			<?php endforeach; ?>
		</div>
	</div>
<?php endif; ?>
<div class="section-block" style="padding-bottom: 0;">
	<div class="container">
		<div class="row" style="margin-top: -110px; z-index: 1028 !important;">
			<?php foreach ($divisions as $division) : ?>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="project-item project-fix" style="z-index: 1029;">
						<img src="<?= substr($division->picture_dir, 7) . $division->picture; ?>" alt="blog-img">
						<div class="project-item-overlay">
							<div class="project-item-content">
								<h5><?= $division->title; ?></h5>
								<p style="color: #fff; margin-top: 10px;">
									<?= $division->text_content; ?>
								</p>
								<a href="<?= $division->link; ?>">
									View More
								</a>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>
<br>
<br>

<div class="section-block">
	<div class="container">
		<div class="row">
			<?php foreach ($videos as $vid) : ?>
				<div class="col-md-6">
					<div class="section-heading-left">
						<h2><?= $vid->title; ?></h2>
					</div>
					<div class="product-box" style="margin-top: 50px;">
						<?php if ($vid->link): ?>
							<div class="product-img" style="border-radius: 10px; height: 315px; overflow: hidden;">
								<iframe width="100%" height="315" src="https://www.youtube.com/embed/<?= explode('=', $vid->link)[1] ?>" frameborder="0" allowfullscreen></iframe>
							</div>
						<?php else: ?>
							<img style="border-radius: 10px;" src="<?= substr($vid->picture_dir, 7) . $vid->picture; ?>" width="100%">
						<?php endif; ?>
						<div class="product-info">
							<p>
								<?= $vid->text_content; ?>
							</p>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
			<div class="col-md-6">
				<div class="section-heading-left">
					<h2>PRODUK UNGGULAN KAMI</h2>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="project-carousel">
							<div class="owl-carousel owl-theme" id="project-detail" style="overflow: hidden;">
								<?php $no = 0 ?>
								<?php foreach ($products as $product): ?>

									<?php if ($no < 5): ?>
										<a href="<?= $this->Url->build(['action' => 'detail', '?' => ['id' => $product->id, 'pages' => 'product']]) ?>">
											<div class="services-box" style="border: none; height: 450px;">
												<img src="<?= substr($product->picture_dir, 7) . $product->picture ?>" alt="blog-img" id="<?= $product->id ?>">
												<div class="services-box-inner">
													<h4><?= $product->name ?></h4>
													<p>
														<?= substr($product->content, 0, 80) . '...'; ?>
													</p>
												</div>
											</div>

										</a>
									<?php else: ?>

									<?php endif ?>
									<?php $no++ ?>
								<?php endforeach; ?>
								<!-- <div class="services-box" style="border: none; height: 520px;">
										<img src="assets/img/content/projects/toroz2.jpg" alt="blog-img">
										<div class="services-box-inner">
											<h4>Toroz Teleradiologi</h4>
											<p>
												Aplikasi yang berbasih Web dan Mobile Android ini dapat membantu para Dokter untuk memberi Diagnosa jarak jauh, dengan mengacu pada data-data yang ditampilkan oleh aplikasi ini dengan akurat...
											</p>
										</div>
									</div>
									<div class="services-box" style="border: none; height: 520px;">
										<img src="assets/img/content/projects/qc-30.jpg" alt="blog-img">
										<div class="services-box-inner">
											<h4>Car Wash System</h4>
											<p>
												Saat ini perkembangan populasi kendaraan di Indonesia sangat signifikan, mengacu pada hal tesebut maka peluang untuk melakukan bisnis terkait...
											</p>
										</div>
									</div> -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="section-block-grey" style="margin-top: 0px;">
	<div class="container">
		<?php foreach ($judul as $judul) : ?>
			<div class="section-heading-left">
				<h2><?= $judul->title; ?></h2>
			</div>
		<?php endforeach; ?>
		<style>
			.customer-display {
				display: flex;
				justify-content: space-between;
			}
			.customer-center {
				position: relative;
				top: 50%;
				left: 50%;
				-ms-transform: translate(-50%, -50%);
				transform: translate(-50%, -50%);
			}
		</style>
		<div id="customers">
			<?php
			$batas = count($b);
			for ($z = 1; $z <= $batas; $z++) {
			?>
				<div>
					<div class="customer-display">
						<?php foreach (array_slice($b[$z], 0, 5) as $foot) : ?>
							<div>
								<img class="customer-center" src="<?= substr($foot->picture_dir, 7) . $foot->picture; ?>" width="150">
							</div>
						<?php endforeach; ?>
					</div>
					<div class="customer-display">
						<?php foreach (array_slice($b[$z], 5, 5) as $foot) : ?>
							<div>
								<img class="customer-center" src="<?= substr($foot->picture_dir, 7) . $foot->picture; ?>" width="150">
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</div>

<?php $this->start('script') ?>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src='/assets/video_js/video.js'></script>
<script src="/assets/video_js/videojs-playlist.js"></script>

<script type="text/javascript">
	$('#customers').slick({
		slidesToShow: 1,
		arrows: false,
		fade: true,
		speed: 1000,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 2000,
	});
</script>

<script>
	setTimeout(function() {
		var height = $('#scroll video').height();
		$('#scroll-overlay').css('height', height);
	}, 3000);
	$('#project-detail').owlCarousel({
		loop: true,
		nav: false,
		dots: false,
		autoplay: true,
		autoplayTimeout: 3000,
		responsiveClass: true,
		autoplayHoverPause: false,
		responsive: {
			0: {
				items: 1,
				margin: 10,
			},
			600: {
				items: 2,
				margin: 25,
			},
			1000: {
				items: 2,
				margin: 25,
			}
		}
	});
	<?php
	$videos = [];
	foreach ($video_cover as $key => $value) {
		$videos[] = [
			'video_file' => str_replace('webroot', '', $value->picture_dir) . $value->picture,
			'file_type' => 'video/mp4'
		];
	}
	?>
	var player = videojs('video');
	var data = <?= json_encode($videos) ?>;
	var videos = [];
	if (data.length == 1) {
		$.each(data, function(i, e) {
			videos.push({
				sources: [{
					src: e.video_file,
					type: e.file_type
				}]
			});
		});
	}
	$.each(data, function(i, e) {
		videos.push({
			sources: [{
				src: e.video_file,
				type: e.file_type
			}]
		});
	});
	player.playlist(videos);
	player.playlist.autoadvance(0);
	player.playlist.repeat(true);

	AOS.init();
</script>
<?php $this->end() ?>