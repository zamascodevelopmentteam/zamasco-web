<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"/>

<div class="owl-carousel owl-theme" id="banner">
<?php foreach($banners as $banner): ?>
  <div class="page-title " style="background-image: url(<?= substr($banner->picture_dir,7).$banner->picture ?>); background-position: center; height: 350px;">
    <div class="container">
     
    </div>
  </div>
<?php endforeach; ?>
</div>

<?php if($pages == 'product'): ?>
  <?php
    foreach ($details as $detail) {} foreach ($logo1 as $logo1) {
      # code...
    } ?>
  <div class="section-block">
    <div class="container">

      <div class="row">

        <div class="col-md-6 col-md-push-6">
          <div class="img-induk owl-carousel owl-theme">
            <?php $no = 0; ?>
            <div class="item" data-hash="<?= $no++; ?>">
              <img src="<?= substr($detail->picture_dir, 7).$detail->picture ?>">              
            </div>
            <?php foreach ($kecil as $kecil) :?>
            <div class="item" data-hash="<?= $no++; ?>">
              <img src="<?= substr($kecil->path, 7).$kecil->name ?>">              
            </div>
            <?php endforeach; ?>
          </div>
          <hr>
          <style>
            .active-img{
              border: 2px solid black;
            }
          </style>
          <div style="text-align: center;">
            <a href="#0"><img class="img-click active-img" style="width: 50px; height: 50px" src="<?= substr($logo1->picture_dir, 7)."thumbnail-".$logo1->picture ?>"></a>
          <?php $i=1; foreach ($logo2 as $logo2) : ?>
            <a href="#<?= $i; ?>"><img class="img-click" src="<?= substr($logo2->path, 7)."thumbnail-".$logo2->name; ?>" style="height: 50px;"></a>
          <?php $i++; endforeach; ?>
          </div>
          <hr>
          <?php if (!$detail->links) : ?>
            
          <?php else : ?>
            <div class="row">
              <?php 
                $video = explode('=', $detail);
               ?>
              <div class="col-md-12">
                <iframe width="100%" height="315" class="img-rounded" src="https://www.youtube.com/embed/<?= $video[1] ?>" frameborder="0" allowfullscreen></iframe>
              </div>
            </div>
          <?php endif ?>
        </div>
        <div class="col-md-6 col-md-pull-6">
          <div class="section-heading-left">
            <h2><?= $detail->name ?></h2>
          </div>
          <span class="label label-primary" title="DIVISI : <?= strtoupper($detail->division) ?>"><?= strtoupper($detail->division) ?></span>
          <span class="label label-success" title="KATEGORI : <?= strtoupper($detail->category) ?>"><?= strtoupper($detail->category) ?></span>
          <br><br>
          <?= $detail->content ?>

          <a href="<?= $this->Url->build(['action'=>'contact']) ?>" class="btn btn-primary pull-right">Pesan Sekarang</a>
          <br><br><br>

          <?php if (!empty($noan->id)) :?>

            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
              <div class="panel">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                      <i class="far fa-file-alt"></i>
                      Brosur
                    </h4>
                  </div>
                </a>
                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                    <?php foreach ($file as $file) : ?>
                      <i class="fas fa-download"></i>&nbsp;
                      <a target="_blank" href="/assets/file/<?= $file->file; ?>"><?= $file->file; ?></a><br>
                    <?php endforeach; ?>
                    </div>
                </div>
              </div>
            </div>

          <?php endif; ?>

        </div>
      </div>
    </div>
  </div>
  <br><br>
<?php elseif($pages == 'gallery'): ?>
  <?php
    foreach ($albums as $album) {
      // code...
    }
  ?>
  <div class="section-block">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="section-heading">
						<center><h2><?= $album->title ?></h2></center>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<div id="container">
            <div id="mygallery" >

              <?php foreach ($details as $detail): ?>
                <!-- <div class="photo item">
                  <a class="lightBoxImg" href="<?= substr($detail->path, 7).$detail->name; ?>">
                    <img src="<?= substr($detail->path, 7).$detail->name ?>" >
                  </a>
                </div> -->

                <div class="photo item">
                  <a href="#" data-izimodal-open="#<?= $detail->id; ?>" data-izimodal-transitionin="fadeInDown" data-izimodal-zindex="20000">
                    <img src="<?= substr($detail->path,7).$detail->name ?>">
                  </a>
                </div>
              <?php endforeach; ?>

            </div>
						
					</div>
				</div>
			</div>

		</div>
	</div>

  <?php foreach ($details as $detail) : ?>

    <div class="modal-custom" data-izimodal-group="team" id="<?= $detail->id; ?>" data-iziModal-fullscreen="true">
      <img src="<?= substr($detail->path, 7).$detail->name ?>" width="100%">
    </div>

  <?php endforeach; ?>
<?php endif; ?>
  <?php $this->start('script') ?>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
  
  <script type="text/javascript">
    $('#customers').slick({
      slidesToShow: 1,
      arrows: false,
      fade: true,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 3000,
    });
  </script>

  <script>
    $('.img-induk').owlCarousel({
      loop: true,
      items: 1,
      autoHeight:true,
      dots: false,
      URLhashListener:true,
      lazyLoad: true,
      autoplayHoverPause:true,
      startPosition: 'URLHash'
    });

    $('.img-anak').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      asNavFor: '.img-induk',
      dots: true,
      centerMode: true,
      focusOnSelect: true
    });
  </script>

  <script>
    $("#mygallery").justifiedGallery({
      rowHeight: 200,
      lastRow: 'nojustify',
      randomize: true,
      margins: 5
    }).on('jg.complete', function(){
      $('.lightBoxImg').swipebox();
    });

    $(document).ready(function(){
      $(".modal-custom").iziModal({
        background: 'transparent',
        arrowKeys: true,
        bodyOverflow: false,
        borderBottom: false,
        navigateArrows: false,
        navigateCaption: false,
        zindex: 30000
      });

      lightbox.option({
          'resizeDuration': 200,
          'wrapAround': true
      })

    });
  </script>

  <script>
    $('.img-click').click(function(){
      $('.img-click').removeClass('active-img');
      $(this).addClass('active-img');
    });
  </script>
  <?php $this->end() ?>