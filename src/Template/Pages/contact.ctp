
<div class="owl-carousel owl-theme" id="banner">
<?php foreach($banners as $banner): ?>
  <div class="page-title " style="background-image: url(<?= substr($banner->picture_dir,7).$banner->picture ?>); background-position: center; height: 350px;">
    <div class="container">
      
    </div>
  </div>
<?php endforeach; ?>
</div>

  <div class="section-block">
    <div class="form-group">
      <div class="container">
          <div class="row">
             <div class="col-md-6 col-sm-12">
                 <div class="section-heading" style="margin-bottom: 20px;">
                  <h2 style="text-align: center;">Kontak</h2>
                </div>

                <?= $this->Form->create() ?>
                    <div class="form-group">
                      <div class="col-md-12">
                        <?=$this->Form->input('name',[
                               'required' => true
                           ]); ?>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-12">
                        <?=$this->Form->input('email',[
                               'required' => true
                           ]); ?>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-12">
                        <?=$this->Form->input('subjek',[
                               'required' => true
                           ]); ?>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-12">
                        <?=$this->Form->input('pesan',[
                              'type' => 'textarea',
                              'rows' => '4',
                              'class' => 'form-control',
                              'required' => true
                           ]); ?>
                      </div>
                    </div>
                     <div class="col-md-12">
                       <button type="submit" class="btn btn-primary"><i style="font-size: 15px;" class="fa fa-envelope"></i>&nbsp;Kirim</button>
                     </div>

                <?= $this->Form->end(); ?>
              </div>
               <div class="col-md-6 col-sm-12">

                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3579.365127199295!2d106.87229081454791!3d-6.165223462137333!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f5057a82f681:0xcf8ecef827946263!2sPT+Zamasco+Mitra+Solusindo!5e1!3m2!1sen!2sid!4v1535683836038" width="600" height="423" frameborder="0" style="border:0" allowfullscreen></iframe>

                <a style="margin-top: 8px;" href="https://maps.google.com?saddr=Current+Location&amp;daddr=PT+Zamasco+Mitra+Solusindo" class="btn btn-primary" target="_blank">
                <i class="fa fa-location-arrow"></i>&nbsp; Petunjuk Arah</a>&nbsp;

               </div>
          </div>
      </div>
    </div>
  </div>

  <!-- video -->

  <!-- <div style="width: 100%; height: 600px;"
    data-vide-bg="<?= substr($video->picture_dir, 7).$video->picture; ?>"
    data-vide-options="position: 50% 50%">
    <div style="padding-top: 15%;">
      <div class="container-fluid">
        <div class="col-md-8 col-md-offset-2" style="text-align: center;">
          <div>
            <h1 class="animated zoomIn animation-delay-100" style="transform: scale(1); font-size: 50px;">
              <?= $video->title; ?>
            </h1>
            <div class="lead animated zoomIn animation-delay-200">
              <?= $video->text_content; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> -->

  <!-- end video -->

  <div class="section-block-grey">
    <div class="container">
      <div class="row">

        <div class="col-md-4">
          <div class="section-heading-left" style="margin-bottom: 30px;">
            <h2>Kontak</h2>
          </div>
          <?php foreach ($alamat as $alamat) : ?>
          <div class="col-md-6">
            <img src="<?= substr($alamat->picture_dir, 7).$alamat->picture; ?>" width="100%">
          </div>
          <div class="col-md-6">
            <p><?= $alamat->text_content; ?></p>
          </div>

          <?php endforeach; ?>
        </div>
        <div class="col-md-3" style="margin-top:90px;">
          <div class="row">
            <?php foreach ($no_sel as $kan) : ?>
              <div class="col-md-2 col-sm-2 col-xs-2">
                <a href="https://api.whatsapp.com/send?phone=<?= $kan->text_content; ?>&text=Halo%20Zamasco" target="_BLANK"><i style="font-size: 30px; color: #34af23" class="fab fa-whatsapp"></i></a>
              </div>
            <?php endforeach; ?>
            <div class="col-md-10 col-sm-10 col-xs-10">
              <div class="row">
                <?php foreach ($no_kan as $kan) : ?>
                <div>Kantor &nbsp;&nbsp; : <span style="color: #0984e3; font-family: 'Times New Roman'">
                    <?php
                      $sps1 = substr($kan->text_content, 0,3);
                      $sps2 = substr($kan->text_content, 3,8);
                      $kan->text_content = $sps1 . " " . $sps2;
                     ?>
                    <?= $kan->text_content; ?>
                  </span>
                </div>
                <?php endforeach; ?>
              </div>
              <div class="row">
                <?php foreach ($no_sel as $sel) : ?>
                <div>Seluler &nbsp; : <span style="color: #0984e3; font-family: 'Times New Roman'">
                    <?= $sel->text_content; ?>
                  </span>
                </div>
                <?php endforeach; ?><br>
              </div>
            </div>
          </div>
          <?php foreach ($email as $email) : ?>
          <div class="row">
            <div class="col-md-2 col-sm-2 col-xs-2">
              <i style="font-size: 20px;" class="fas fa-envelope"></i>
            </div>
            <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <div>Email &nbsp;&nbsp;&nbsp;&nbsp; : <span style="color: #0984e3;">
                      <?= $email->text_content ?>
                    </span>
                  </div>
                </div>
            </div>
          </div>
          <?php endforeach; ?>
        </div>


        <div class="col-md-5">
          <br>
          <div class="row">
            <div class="col-md-12" style="margin-right: -20px;">
              <img src="assets/img/logos/zamasco-image.png" width="80%">
            </div>
          </div><br>
          <?php foreach ($cont as $content) : ?>
          <div class="row">
            <div class="col-md-12">
                <p><?php echo $content->text_content; ?></p>
            </div>
          </div>
          <div class="row">
            <div class="col-md-2">
              <a href="<?= substr($content->picture_dir, 7).$content->picture; ?>" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="<?= $content->title; ?>">Download</a>
            </div>
          </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>

  <?php $this->start('script'); ?>
    <script>
      $(function () {
        $('[data-toggle="tooltip"]').tooltip()
      })
    </script>
  <?php $this->end(); ?>