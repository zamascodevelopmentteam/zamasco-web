<div class="owl-carousel owl-theme" id="banner">
	<?php foreach($banners as $banner): ?>
		<div class="page-title " style="background-image: url(<?= substr($banner->picture_dir,7).$banner->picture ?>);  background-position: center; height: 350px;">
			<div class="container">

			</div>
		</div>
	<?php endforeach; ?>
</div>

<style>
	hr{
		border: 1.5px solid grey;
	}
</style>

<div class="section-block">
	<div class="container">

		<!-- Career -->

		<div class="section-heading" style="text-align: center;">
			<h2 style="font-size: 50px;">Join Our Team</h2>
		</div>
		<?= $this->Flash->render() ?>

		<span style="font-size: 30px;">Available Position : </span>
		<br><br><br>
		<?php foreach ($cont as $cont) : ?>
			<div class="row">
				<div class="col-md-3">
					<img src="<?= substr($cont->picture_dir, 7).$cont->picture; ?>" width="100%">
				</div>

				<div class="col-md-9">
					<div>
						<h2>
							<?= $cont->title; ?>
						</h2>
						<hr>
						<div>
							<?= $cont->text_content; ?>
						</div>
						<div style="margin-top: 20px;">
							<button class="btn btn-primary btn-apply" data-name="<?= $cont->title; ?>" data-toggle="modal" data-target="#myModal">Apply Now!</button>
						</div>
					</div>
				</div>
			</div>
			<br><br><br>
			<br><br>
		<?php endforeach; ?>

		<!-- end career -->

	</div>
</div>
<div class="modal fade" id="myModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<?= $this->Form->create($recruter,['class'=>'m-form m-form--fit m-form--label-align-right', 'type' => 'file']) ?>
				<!-- Modal Header -->
				<div class="modal-header">
					<h4 class="modal-title"></h4>
				</div>

				<!-- Modal body -->
				<div class="modal-body">
					<?= $this->Form->control('name',[
						'label' => 'Nama',
						'required' => true
					]) ?>
					<?= $this->Form->control('phone',[
						'label' => 'No. Telepon',
						'type' => 'number',
						'required' => true
					]) ?>
					<?= $this->Form->control('address',[
						'label' => 'Alamat',
						'type' => 'textarea',
						'class' => 'form-control',
						'required' => true
					]) ?>
					<?= $this->Form->control('education',[
						'label' => 'Pendidikan Terakhir',
						'options' => [
							'SMK' => 'SMK',
							'Diploma 1' => 'Diploma 1',
							'Diploma 2' => 'Diploma 2',
							'Diploma 3' => 'Diploma 3',
							'Sarjana 1' => 'Sarjana 1',
							'Sarjana 2' => 'Sarjana 2',
							'Sarjana 3' => 'Sarjana 3'
						],
						'empty' => "Pilih Pendidikan Terakhir",
						'required' => true
					]) ?>
					<?= $this->Form->control('file',[
						'label' => 'Curriculum Vitae (CV)',
						'type' => 'file',	
						'required' => true
					]) ?>
					<div style="display: none;">
						<?= $this->Form->control('job_title') ?>
					</div>
				</div>

				<!-- Modal footer -->
				<div class="modal-footer">
					<button class="btn btn-primary">Submit</button>
					<button type="reset" class="btn btn-primary" data-dismiss="modal">Close</button>
				</div>
			<?= $this->Form->end();?>
		</div>
	</div>
</div>
<?php $this->start('script') ?>
<script>
	$('.btn-apply').click(function(event) {
		event.preventDefault();
		var e = $(this);
		$('.modal-title').text(e.data('name'));
		$('#job-title').val(e.data('name'));
		$('.custom-file-label').css('display', 'none');
	});
	
</script>
<?php $this->end() ?>
