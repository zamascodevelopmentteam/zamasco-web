	<div class="owl-carousel owl-theme" id="banner">
	<?php foreach($banners as $banner): ?>
		<div class="page-title " style="background-image: url(<?= substr($banner->picture_dir,7).$banner->picture ?>); background-position: center;">
			<div class="container">
				<h1><?= ucfirst(strtolower($banner->pages)) ?></h1>
				<ul>
					<li><a href="<?= $this->url->build(['action'=>'index']) ?>">Home</a></li>
					<li><a href="<?= $this->url->build(['action'=>strtolower($banner->pages)]) ?>"><?= ucfirst(strtolower($banner->pages)) ?></a></li>
				</ul>
			</div>
		</div>
	<?php endforeach; ?>
	</div>


	<div class="section-block" style="background-color: rgb(242,242,242); margin-bottom: -30px;">

		<!-- Begin Division -->

		<div class="container">
			<center><h3>- DIVISION -</h3></center><br>
			<div class="filters">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<div class="row">
					    	<div class="col-xs-6 col-sm-3">
					      		<center><a id="division1" href="#!"  class="button btn-custom-black btn-custom-active" data-filter="">All</a></center>
					      	</div>
					      	<div class="col-xs-6 col-sm-3">
					      		<center><a id="division2" href="#ehealth" class="button btn-custom-black" data-filter=".ehealth">e-health</a></center>
					      	</div>
					      	<div class="col-xs-6 col-sm-3">
					      		<center><a id="division4" href="#consultant" class="button btn-custom-black" data-filter=".consultant">consultant</a></center>
					      	</div>
					      	<div class="col-xs-6 col-sm-3">
					      		<center><a id="division3" href="#it" class="button btn-custom-black" data-filter=".it">technology</a></center>
					      	</div>
					    </div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="section-block">
		<div class="container">
		<!-- End Division -->
			<div class="bungkus">
				<!-- Begin Category -->

			<div class="grid-category test">

				<div class="col-md-10 col-md-offset-1 category-item it test" style="display: none">
					<div class="ui-group">
						<!-- <fieldset> -->
							<!-- <legend>Category</legend> -->
								<div class="row">
									<div class="col-md-2 col-md-push-2 col-xs-6 ">
										<center> <button class="button btn-custom-black" data-filter=".web">web</button></center>
									</div>
									<div class="col-md-2 col-md-push-2 col-xs-6 ">
										<center><button class="button btn-custom-black" data-filter=".mobile">mobile</button></center>
									</div>
									<div class="col-md-2 col-md-push-2 col-xs-6 ">
										<center><button class="button btn-custom-black" data-filter=".software">software</button></center>
									</div>
									<div class="col-md-2 col-md-push-2 col-xs-6 ">
										<center><button class="button btn-custom-black" data-filter=".hardware">hardware</button></center>
									</div>
								</div>

						<!-- </fieldset> -->
				  	</div>

				</div>


				<div class="col-md-8 col-md-offset-2 category-item ehealth test" style="display: none">

					<div class="ui-group">
						<!-- <fieldset> -->
							<!-- <legend>Category</legend> -->
								<div class="row">

									<div class="col-md-4 col-md-push-2 col-xs-6 ">
										<center> <button class="button btn-custom-black" data-filter=".telemedicine">telemedicine</button></center>
									</div>
									<div class="col-md-4 col-md-push-2 col-xs-6 ">
										<center><button class="button btn-custom-black" data-filter=".teleradiologi">teleradiologi</button></center>
									</div>
								</div>

						<!-- </fieldset> -->
				  	</div>

				</div>


				<div class="col-md-8 col-md-offset-2 category-item consultant test" style="display: none">

					<div class="ui-group">
						<!-- <fieldset> -->
							<!-- <legend>Category</legend> -->
								<div class="row">

									<div class="col-md-4 col-md-push-2">
										<center> <button class="button btn-custom-black" data-filter=".rumahsakit">consultant managemen rumah sakit</button></center>
									</div>
									<div class="col-md-4 col-md-push-2">
										<center><button class="button btn-custom-black" data-filter=".global">Survey Solution</button></center>
									</div>
								</div>

						<!-- </fieldset> -->
				  	</div>

				</div>
			</div>

			<!-- End Category -->
		<br>
			<!-- Begin Product Content -->

				<div class="grid-product test" style="transition: 1s;">
					<div class="row">

						<?php foreach($products as $product ): ?>

							<div class="col-md-4 col-sm-6 col-xs-12 product-item <?= $product->division ?> <?= $product->category ?> ">
							    <div class="thumbnail">

							        <img src="<?= substr($product->picture_dir, 7).$product->picture ?>" alt="blog-img"  id="<?= $product->id ?>">
							        <div class="caption" id="<?= $product->id ?>">

							        	<h4><?= $product->name ?></h4><br>
											<?= substr($product->content,0,150)."..." ?>

										<div class="footer">
											<a href="detail?id=<?= $product->id ?>&pages=product" class="btn btn-primary pull-right ">
												Lihat Detail
											</a>
										</div>

										<br>
							        </div>

							    </div>
							 </div>
								<script type="text/javascript">
											if($('#<?= $product->id ?>').height() < 250){
												var height = $('#<?= $product->id ?>').height();
													$('#<?= $product->id ?>').css({'margin-top' : (293-height)/2 + 'px','margin-bottom' : (293-height)/2 + 'px'});
											}
								</script>
						<?php endforeach ?>
						<!-- <div class="col-md-4 col-sm-6 col-xs-12 product-item IT WEB ">
						    <div class="thumbnail">

						        <img src="assets/img/content/projects/award.jpg" alt="blog-img" >
						        <div class="caption">

						        	<h4>Award Website</h4>
									<p style="margin-top: 10px;">
										Dalam membangun sebuah website dengan tujuan pengelola membari penghargaan tertentu terkait perorangan atau organisasi, maka titik berat pada website ini memberikan informasi yang jelas kepada pendaftar, dengan mengedepankan kemudahan navigasi, serta menu menu yang mudah di akses dan mudah dipahami.
									</p>
									<div class="footer">
										<a href="products/award-website.html" class="btn btn-primary pull-right ">
											View More
										</a>
									</div>

									<br>
						        </div>

						    </div>
						 </div> -->





					</div>


				</div>

			<!-- End Product Content -->
			</div>


		</div>

	</div>

	<?php $this->start('script') ?>

	<script>

		$('#banner').owlCarousel({
		    items:1,
		    smartSpeed:450,
		    loop: true,
		    autoplay:true,
		    autoplayTimeout:4000,
		    autoplayHoverPause:true,
		    lazyLoad: true,
		    dots: false,
		    autoHeight:true
		        // autoWidth:true,
		  });


		$('.grid-category').isotope({
			filter: '.all'
		});



		$( window ).on( 'hashchange', function( e ) {
			var hash = location.hash.substr(1);
		    if (hash == "") {
				$('.grid-category').isotope({
					filter: '.all'
				});
				// $('.grid-product').animate({top: '0%'});

				$('.ui-group button').removeClass('btn-custom-active');
				$('.all').addClass('btn-custom-active');

			}else if(hash == "ehealth"){
				$('.grid-category').isotope({
					filter: '.ehealth'
				});
				$('.grid-product').isotope({
					itemSelector: '.product-item',
					filter: '.telemedicine, .teleradiologi, .survey'
				});

				$('.filters a').removeClass('btn-custom-active');
				$('#division2').addClass('btn-custom-active');

				$('.ui-group button').removeClass('btn-custom-active');
				$('.all').addClass('btn-custom-active');

			}if(hash == "it"){
				$('.grid-category').isotope({
					filter: '.it'
				});
				$('.grid-product').isotope({
					itemSelector: '.product-item',
					filter: '.web, .mobile, .software, .hardware'
				});

				$('.filters a').removeClass('btn-custom-active');
				$('#division3').addClass('btn-custom-active');

				$('.ui-group button').removeClass('btn-custom-active');
				$('.all').addClass('btn-custom-active');

			}if(hash == "consultant"){
				$('.grid-category').isotope({
					filter: '.consultant'
				});
				$('.grid-product').isotope({
					itemSelector: '.product-item',
					filter: '.rumahsakit, .global'
				});

				$('.filters a').removeClass('btn-custom-active');
				$('#division4').addClass('btn-custom-active');

				$('.ui-group button').removeClass('btn-custom-active');
				$('.all').addClass('btn-custom-active');

			}
		} );



		$('.grid-category').isotope({
		  itemSelector: '.category-item',
		  layoutMode: 'fitRows'

		})

		$('.filters a').click(function(){
			$('.filters a').removeClass('btn-custom-active');
			$(this).addClass('btn-custom-active');

			$('.grid-product').addClass('animated zoomIn faster').one('webkitAnimationEnd mozAnimationEnd oAnimationEnd animationend',
				function(){
					$(this).removeClass('animated zoomIn faster');
				});


			var selectorCategory = $(this).attr('data-filter');
			if(selectorCategory == ""){
				$('.grid-category').isotope({
					filter: '.all'
				});


			}else{
				$('.grid-category').isotope({
					filter: selectorCategory
				});

				$('.ui-group button').removeClass('btn-custom-active');
				$('.all').addClass('btn-custom-active');
			}


			$('.grid-product').isotope({
				itemSelector: '.product-item',
				filter: selectorCategory
			});
		});

	</script>

	<script>
		var hash = location.hash.substr(1);
		    if (hash == "") {
				$('.grid-category').isotope({
					filter: '.all'
				});
				// $('.grid-product').animate({top: '0%'});



			}else if(hash == "ehealth"){
				$('.grid-category').isotope({
					filter: '.ehealth'
				});
				$('.grid-product').isotope({
					itemSelector: '.product-item',
					filter: '.telemedicine, .teleradiologi, .survey'
				});

				$('.filters a').removeClass('btn-custom-active');
				$('#division2').addClass('btn-custom-active');

			}if(hash == "it"){
				$('.grid-category').isotope({
					filter: '.it'
				});
				$('.grid-product').isotope({
					itemSelector: '.product-item',
					filter: '.web, .mobile, .software, .hardware'
				});

				$('.filters a').removeClass('btn-custom-active');
				$('#division3').addClass('btn-custom-active');

			}if(hash == "consultant"){
				$('.grid-category').isotope({
					filter: '.consultant'
				});
				$('.grid-product').isotope({
					itemSelector: '.product-item',
					filter: '.rumahsakit, .global'
				});

				$('.filters a').removeClass('btn-custom-active');
				$('#division4').addClass('btn-custom-active');

			}

	</script>


	<script>
		$('.ui-group button').click(function(){
			$('.ui-group button').removeClass('btn-custom-active');
			$(this).addClass('btn-custom-active');

			var selectorProduct = $(this).attr('data-filter');
			$('.grid-product').isotope({
				itemSelector: '.product-item',
				filter: selectorProduct
			});
		});
	</script>

	<?php $this->end() ?>
