	<style>
		.ul li {
			width: 2em;
			height: 2em;
			text-align: center;
			line-height: 1.7em;
			border-radius: 1em;
			border: 2px solid dodgerblue;
			background-color: white;
			display: inline-block;
			color: dodgerblue;
			position: relative;
		}

		.ul .balok {
			width: 50%;
			margin: 0 -3em;
		}

		.ul .margin {
			margin-left: 73%;
		}

		.ul li::before {
			content: '';
			position: absolute;
			top: .8em;
			left: -10em;
			width: 10em;
			height: .2em;
			background: dodgerblue;
			z-index: -1;
		}


		.ul li:first-child::before {
			display: none;
		}

		.active~li {
			background: lightblue;
		}

		.active~li::before {
			background: lightblue;
		}


		/*Right Structur*/


		.ol li {
			width: 7em;
			height: 2em;
			text-align: center;
			line-height: 1.7em;
			border-radius: 1em;
			border: 2px solid dodgerblue;
			background-color: white;
			display: inline-block;
			color: dodgerblue;
			position: relative;
		}

		.ol .balok {
			width: 13%;
			margin: 0 -3em;
		}

		.ol .margin {
			margin-left: 79%;
		}

		.ol li::before {
			content: '';
			position: absolute;
			top: .8em;
			left: -10em;
			width: 10em;
			height: .2em;
			background: dodgerblue;
			z-index: -1;
		}


		.ol li:first-child::before {
			display: none;
		}

		.active~li {
			background: lightblue;
		}

		.active~li::before {
			background: lightblue;
		}
	</style>

	<div class="owl-carousel owl-theme" id="banner">
		<?php foreach ($banners as $banner): ?>
			<div class="page-title" style="background-image: url('<?= substr($banner->picture_dir, 7) . $banner->picture ?>'); background-position: center; height: 350px;">
			</div>
		<?php endforeach; ?>
	</div>

	<div class="section-block">
		<div class="container-fluid">
			<div class="row">

				<!-- Introduce -->

				<div class="container">
					<div class="row">
						<div class="col-md-12 col-xs-12">
							<div class="col-md-4 col-xs-10">
								<img src="<?= substr($logoVisiMisi->picture_dir, 7) . $logoVisiMisi->picture ?>" width="200" class="img-responsive img-rounded">
							</div>
							<div class="col-md-4 col-xs-10">
								<div class="section-heading-left">
									<h2>Visi</h2>
								</div><br>
								<?php foreach ($visi as $visi) : ?>
									<div class="col-md-12 col-xs-12">
										<p style="white-space: pre-wrap;"><?= $visi->text_content; ?></p>
									</div>
								<?php endforeach; ?>
							</div>
							<div class="col-md-4 col-xs-10">
								<div class="section-heading-left">
									<h2>Misi</h2>
								</div><br>
								<?php foreach ($misi as $misi) : ?>
									<div class="col-md-12 col-xs-12">
										<p style="white-space: pre-wrap;"><?= $misi->text_content; ?></p>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div><br><br>

				<!-- end Introfuce -->

				<!-- Division -->

				<div class="row">
					<?php foreach ($tech as $tech) : ?>
						<div id="img-1" style="background-image: url('<?= substr($tech->picture_dir, 7) . $tech->picture; ?>')" class="img-responsive">
							<div class="container" data-aos="fade-down" data-aos-delay="500">
								<br>
								<div class="row">
									<div class="col-md-12">
										<h1 style="color: white; font-size: 45px; text-align: right;"><?= $tech->title; ?></h1>
									</div>
								</div>

								<div class="row">
									<div class="col-md-12 text-right">
										<br>
										<p style="font-size: 18px;">
											<?= $tech->text_content; ?>
										</p>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<br><br><br>

										<div class="text-right">
											<div class="row">
												<img src="assets/img/content/about/Picture1.png" width="50"> &nbsp;&nbsp;
												<img src="assets/img/content/about/laravel-2.png" width="50"> &nbsp;&nbsp;
												<img src="assets/img/content/about/Picture3.png" width="50"> &nbsp;&nbsp;
												<img src="assets/img/content/about/Picture8.png" width="50"> &nbsp;&nbsp;
												<img src="assets/img/content/about/Picture5.png" width="60"> &nbsp;&nbsp;
												<img src="assets/img/content/about/Picture7.png" width="60"> &nbsp;&nbsp;
												<img src="assets/img/content/about/Picture4.png" width="60"> &nbsp;&nbsp;
												<img src="assets/img/content/about/Picture9.png" width="50"> &nbsp;&nbsp;
												<img src="assets/img/content/about/Picture10.png" width="50"> &nbsp;&nbsp;
												<img src="assets/img/content/about/Picture11.png" width="40"> &nbsp;&nbsp;
												<img src="assets/img/content/about/Picture12.png" width="40"> &nbsp;&nbsp;
												<img src="assets/img/content/about/android.png" width="40">
											</div>
										</div>
									</div>
								</div>
							</div><br><br><br>
						</div>
					<?php endforeach; ?>
				</div>
				<div class="row">
					<?php foreach ($ehealth as $eh) : ?>
						<div id="img-2" style="background-image: url('<?= substr($eh->picture_dir, 7) . $eh->picture; ?>');" class="img-responsive">
							<br><br>

							<div class="container" data-aos="fade-down" data-aos-delay="500">

								<div class="row">
									<div class="col-md-12">
										<h1 style="color: white; font-size: 45px; text-align: left;"><?= $eh->title; ?></h1>
									</div>
								</div>

								<div class="row">
									<div class="col-md-12 text-left">
										<br>
										<p style="font-size: 18px;">
											<?= $eh->text_content; ?>
										</p>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<br>
										<!-- <div style="text-align: center; font-size: 30px;">Our Partner</div> -->
										<br><br>

										<div class="text-left">
											<img src="assets/img/content/about/zte.png" width="50">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<img src="assets/img/content/about/eztalks.png" width="120">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<img src="assets/img/content/about/ecatalog.png" width="120">
										</div>
									</div>
								</div>

							</div><br><br><br><br>
						</div>
					<?php endforeach; ?>
				</div>
				<div class="row">
					<?php foreach ($consultant as $cons) : ?>
						<div id="img-3" style="background-image: url('<?= substr($cons->picture_dir, 7) . $cons->picture; ?>');" class="img-responsive">
							<br>

							<div class="container" data-aos="fade-down" data-aos-delay="500">

								<div class="row">
									<div class="col-md-12">
										<h1 style="color: white; font-size: 45px; text-align: right;"><?= $cons->title; ?></h1>
									</div>
								</div>

								<div class="row">
									<div class="col-md-12 text-right">
										<br>
										<p style="font-size: 18px;">
											<?= $cons->text_content; ?>
										</p>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<br>
										<!-- <div style="text-align: center; font-size: 30px;">Collaborate</div> -->
										<br><br>

										<div class="text-right">
											<img src="assets/img/content/about/ikkesindo.png" width="90">
										</div>

									</div>
								</div>

							</div><br><br><br><br>
						</div>
					<?php endforeach; ?>
				</div><br><br>

				<!-- end Division -->

				<!-- who we are -->

				<div class="container">
					<?php foreach ($moto as $moto) : ?>
						<div style="text-align: center;" class="section-heading">
							<h2><?= $moto->title; ?></h2>
						</div>
						<div class="row" style="margin-top: -40px;">
							<div class="col-md-6 col-md-offset-3">
								<p style="text-align: center;">
									<?= $moto->text_content; ?>
								</p>
							</div>
						</div><br><br>
					<?php endforeach; ?>
					<div class="row">
						<div class="col-md-12">
							<?php foreach ($moto1 as $moto1) : ?>
								<div class="col-md-4" style="text-align: center;">
									<img src="<?= substr($moto1->picture_dir, 7) . $moto1->picture; ?>" width="80" height="80" style="width: 80px;height: 80px;">
									<h3><?= $moto1->title; ?></h3>
									<p>
										<?= $moto1->text_content; ?>
									</p>
								</div>
							<?php endforeach; ?>
						</div>
					</div><br><br>
				</div>

				<!-- end who we are -->

			</div>
		</div>
	</div><br>

	<div class="section-block-grey">

		<!-- Team -->

		<div class="container">
			<div style="text-align: center;" class="section-heading">
				<h2>Board of Commisioner and Director</h2>
			</div>
			<div class="row">

				<?php foreach ($anggota as $agg) : ?>
					<div class="col-md-4">
						<div class="row">
							<div class="col-md-12">
								<div class="thumbnail" style="height: 410px;border-radius: 10px;">
									<center>
										<img style="height: 300px;border-radius: 10px;" src="<?= substr($agg->picture_dir, 7) . $agg->picture; ?>" height="300">
									</center>
									<div class="caption">
										<h4 class="text-center" style="margin-top: 10px;<?= strlen($agg->title) > 31 ? 'font-size: 14px;' : '' ?>"><?= $agg->title; ?></h4>
										<hr style="margin-top: 14px;margin-bottom: 14px;">
										<center>
											<p style="color: #8395a7; font-size: 15px;"><?= $agg->text_content; ?></p>
										</center>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach; ?>

				<br><br>

				<!-- <div class="col-md-12">
						<div class="row">
							<?php foreach ($komisaris as $kom) : ?>
								<div class="col-md-4">
									<img src="<?= substr($kom->picture_dir, 7) . $kom->picture; ?>" width="100%" class="img-responsive img-rounded">
								</div>
								<div class="col-md-4" style="padding-top: 5%;">
									<h4 style="font-size: 20px; text-align: center;"><?= $kom->title; ?></h4><hr>
									<p style="color: #8395a7; font-size: 20px; text-align: center;">Commissioner</p>
									<br><br>
									<p class="text-center">
										<?= $kom->text_content; ?>
									</p>
								</div>
							<?php endforeach; ?>
							<div class="col-md-4">
								<div class="diamondswrap text-center">
								    <?php foreach ($anggota as $ang) : ?>
								    	<div class="item">
									    	<a href="#" data-izimodal-open="#<?= $ang->id; ?>" data-izimodal-transitionin="fadeInDown" data-izimodal-zindex="20000">
									    		<img src="<?= substr($ang->picture_dir, 7) . $ang->picture ?>">
									    	</a>
									    </div>
								    <?php endforeach; ?>
							    </div>
							</div>
						</div>
					</div> -->
			</div>

			<!-- <?php foreach ($anggota as $ang) : ?>
					<div class="modal-custom" data-izimodal-group="team" id="<?= $ang->id; ?>" data-iziModal-fullscreen="false"  data-iziModal-title="<?= $ang->title; ?>"  data-iziModal-subtitle="<?= $ang->text_content; ?>">
						<img src="<?= substr($ang->picture_dir, 7) . $ang->picture ?>" width="100%">
					</div>
				<?php endforeach; ?> -->

		</div>

		<!-- end -->

	</div>
	<style>
		.custom-margin-0 {
			margin-bottom: 65px;
		}

		.custom-margin-1 {
			margin-top: 63px;
		}
	</style>
	<div><br><br>
		<div class="container">
			<div style="text-align: center;" class="section-heading">
				<h2><?= $foteam->title; ?></h2>
			</div>
			<div class="row">
				<div class="col-md-6 col-md-push-3 col-sm-12 col-xs-12">
					<img src="<?= substr($foteam->picture_dir, 7) . $foteam->picture; ?>" width="100%" class="img-rounded">
				</div>

				<div class="col-md-3 col-md-pull-6 col-sm-12 col-xs-12">
					<?php $no = 1; ?>
					<?php foreach ($b[2] as $struko) : ?>
						<ul class="ul custom-margin-1">
							<li class="balok"><?= $struko->title; ?></li>
							<li class="margin"><?= $struko->text_content; ?></li>
						</ul>
					<?php $no++;
					endforeach; ?>
					<br>
				</div>


				<div class="col-md-3 col-sm-12 col-xs-12">
					<?php $no = 1; ?>
					<?php foreach ($b[1] as $struk) : ?>
						<ul class="ol <?= ($no < 4 ? 'custom-margin-0' : '') ?>">
							<li class="balok"><?= $struk->text_content; ?></li>
							<li class="margin"><?= $struk->title; ?></li>
						</ul>
					<?php $no++;
					endforeach; ?>
					<br>
				</div>
			</div>
		</div>
	</div>

	<br><br>

	<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
	<?php $this->start('script') ?>
	<script>
		$('#banner').owlCarousel({
			items: 1,
			smartSpeed: 450,
			loop: true,
			autoplay: true,
			autoplayTimeout: 4000,
			autoplayHoverPause: true,
			lazyLoad: true,
			dots: false,
			autoHeight: true
			// autoWidth:true,
		});

		$(document).ready(function() {
			$(".modal-custom").iziModal({
				headerColor: 'rgba(0,0,0, .8)',
				background: 'transparent',
				arrowKeys: true,
				borderBottom: false,
				navigateArrows: true,
				navigateCaption: false,
				zindex: 30000
			});

			$(".diamondswrap").diamonds({
				size: 130, // Size of the squares
				gap: 6 // Pixels between squares
			});

			AOS.init();

			lightbox.option({
				'resizeDuration': 200,
				'wrapAround': true
			})
		});
	</script>
	<?php $this->end(); ?>