<div class="owl-carousel owl-theme" id="banner">
<?php foreach($banners as $banner): ?>
	<div class="page-title " style="background-image: url(<?= substr($banner->picture_dir,7).$banner->picture ?>); background-position: center; height: 350px;">

	</div>
<?php endforeach; ?>
</div>

<br>
	<div class="section-block" style="margin-top: -30px">
		<div class="container">

			<div class="content" id="">
				<div class="row">

					<?php foreach($albums as $album): ?>
						<div class="col-md-3 col-sm-6 col-xs-12 content-item" id="<?= $album->id ?>" style="padding: 5px;margin: 0;overflow: hidden;">
							<a href="detail?id=<?= $album->id ?>&pages=gallery">
								<img class="image-gallery-slide" width="100%" src="<?= substr($album->image_dir,7).$album->image ?>" alt="">
								<div class="overlay-slide">
								    <div class="text"><?= $album->title ?></div>
								</div>
							</a>
						</div>
						
					<?php endforeach; ?>

				</div>
			</div>

		</div>
	</div>

	<?php $this->start('script') ?>

	<script>
		$("#container").sliphover();
	</script>
	<script>
		$('img').load(function(){
			$('.content').isotope({
			  itemSelector: '.content-item',
			  percentPosition: true,
			  masonry: {
			    // use outer width of grid-sizer for columnWidth
			    columnWidth: '.content-item',
			  }
			})
		});
		$('.content').isotope({
		  itemSelector: '.content-item',
		  percentPosition: true,
		  masonry: {
		    // use outer width of grid-sizer for columnWidth
		    columnWidth: '.content-item',
		  }
		})
	</script>

	<script>
		var n = 1
		$('.overlay-slide').each(function(){
			if($(this).width() < 200){
				$(this).css('width','45%');
			}
		});

	</script>

	<?php $this->end() ?>
