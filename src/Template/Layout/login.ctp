
<!DOCTYPE html>
<html lang="en">
<head>
  <title><?=$defaultAppSettings['App.Name'];?><?=(!empty($titleModule) ? ' | '.$titleModule : '');?></title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="<?=$defaultAppSettings['App.Name'];?>">
    <meta name="keywords" content="<?=$defaultAppSettings['App.Name'];?>">
    <meta name="author" content="Iqbal Ardiansyah">
    <link rel="shortcut icon" href="<?=$this->Utilities->generateUrlImage(null,$defaultAppSettings['App.Favico']);?>" />
    <!-- Favicon icon -->
    <?php
        $cssExternal = [];
        $cssDefault = [
            'dist/vendors/base/vendors.bundle.css',
            'dist/demo/default/base/style.bundle.css',
        ];
        
        $cssMain = [
            
        ];
        
        $this->Html->css($cssExternal,['block'=>'cssExternal']);
        $this->Html->css($cssDefault,['block'=>'cssDefault','pathPrefix' => '/assets/']);
        $this->Html->css($cssMain,['block'=>'cssMain','pathPrefix' => '/assets/']);
    
        echo $this->fetch('cssExternal');
        echo $this->fetch('cssDefault');
        echo $this->fetch('cssPlugin');
        echo $this->fetch('cssMain');
    ?>

</head>
<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
    <!-- begin:: Page -->
    <div class="m-grid m-grid--hor m-grid--root m-page">
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-3" id="m_login" style="background-image: url(<?=$this->request->base;?>/assets/dist/app/media/img//bg/bg-2.jpg);">
        <div class="m-grid__item m-grid__item--fluid	m-login__wrapper">
            <div class="m-login__container">
                <div class="m-login__logo">
                    <a href="#">
                    <img src="<?=$this->Utilities->generateUrlImage(null,$defaultAppSettings['App.Logo.Login']);?>">  	
                    </a>
                </div>
                <div class="m-login__signin">
                    <div class="m-login__head">
                        <h3 class="m-login__title">Sign In To <?=$defaultAppSettings['App.Name'];?></h3>
                    </div>
                    <?=$this->fetch('content');?>
                </div>
            </div>	
        </div>
    </div>
                        
                    
    <?php
        $jsDefault = [
            'dist/vendors/base/vendors.bundle.js',
            'dist/demo/default/base/scripts.bundle.js',
            'dist/snippets/custom/pages/user/login.js',
        ];
        $jsMain = [
            
        ];
        $this->Html->script($jsDefault,['block'=>'jsDefault','pathPrefix' => '/assets/']);
        $this->Html->script($jsDefault,['block'=>'jsPlugin','pathPrefix' => '/assets/']);
        $this->Html->script($jsMain,['block'=>'jsMain','pathPrefix' => '/assets/']);

        echo $this->fetch('jsDefault');
        echo $this->fetch('jsPlugin');
        echo $this->fetch('jsMain');
        echo $this->fetch('script');
    ?>
    <script>
        
    </script>
</body>
</html>