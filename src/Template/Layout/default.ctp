<!DOCTYPE html>
<html lang="en">

<head>
    <title><?= $defaultAppSettings['App.Name']; ?><?= (!empty($titleModule) ? ' | ' . $titleModule : ''); ?></title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="<?= $defaultAppSettings['App.Name']; ?>">
    <meta name="keywords" content="<?= $defaultAppSettings['App.Name']; ?>">
    <meta name="author" content="Iqbal Ardiansyah">
    <link rel="shortcut icon" href="<?= $this->Utilities->generateUrlImage(null, $defaultAppSettings['App.Favico']); ?>" />
    <!-- Favicon icon -->

    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/font-awesome.css">
    <link rel="stylesheet" href="/assets/css/icomoon.css">
    <link rel="stylesheet" href="/assets/css/pogo-slider.min.css">
    <link rel="stylesheet" href="/assets/css/slider.css">
    <link rel="stylesheet" href="/assets/css/animate.css">
    <link rel="stylesheet" href="/assets/css/owl.carousel.css">
    <link rel="stylesheet" href="/assets/css/lightbox.css">
    <link rel="stylesheet" href="/assets/css/default.css">
    <link rel="stylesheet" href="/assets/css/styles.css">
    <link rel="stylesheet" href="/assets/css/iziModal.min.css">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/diamonds.css" />
    <link rel="stylesheet" href="/assets/css/justifiedGallery.min.css">
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <style type="text/css">
        .services-box a:hover {
            background-color: #ffffff;
            border: 2px solid #eee;
            color: #2578ff;
        }
    </style>
</head>

<body>

    <div style="position: fixed; z-index: 999; right: 20px; bottom: 20px;" id="cta-wa">
        <a href="https://wa.me/6281285001001"><img width="70" src="/img/wa.gif"></a>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/assets/js/jquery.pogo-slider.min.js"></script>
    <script src="/assets/js/pogo-main.js"></script>
    <script src="/assets/js/owl.carousel.js"></script>
    <script type="text/javascript" src="/assets/js/wow.min.js"></script>
    <script type="text/javascript" src="/assets/js/lightbox.js"></script>
    <script src="/assets/js/jquery.counterup.min.js"></script>
    <script src="/assets/js/waypoints.min.html"></script>
    <script type="text/javascript" src="/assets/js/isotope.pkgd.min.js"></script>
    <script type="text/javascript" src="/assets/js/jquery.circliful.min.js"></script>
    <script type="text/javascript" src="/assets/js/tabs.min.js"></script>
    <script type="text/javascript" src="/assets/js/iziModal.min.js"></script>
    <script src="/assets/js/modernizr.js"></script>
    <script type="text/javascript" src="/assets/js/jquery.diamonds.js"></script>
    <script src="/assets/js/jquery.justifiedGallery.min.js"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="/assets/js/jquery.vide.js"></script>
    <script src="/assets/js/dropzone.js"></script>
    <?= $this->element('main'); ?>

    <script src="/assets/js/main.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-150948441-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-150948441-1');
    </script>
    <?= $this->fetch('script'); ?>
</body>

</html>