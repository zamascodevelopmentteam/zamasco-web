<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Album $album
 */
?>

<div class="m-portlet  m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_1">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                <?=$titlesubModule;?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="<?=$this->Url->build(['action'=>'index']);?>" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Back" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-left"></i></a>
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a>
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a>
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#" m-portlet-tool="remove" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-close"></i></a>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <table class="table">
          <tr>
              <th scope="row"><?= __('Title') ?></th>
              <td><?= h($album->title) ?></td>
          </tr>
          <tr>
              <th scope="row"><?= __('Image') ?></th>
              <td><?= h($album->image) ?></td>
          </tr>
          <tr>
              <th scope="row"><?= __('Image Dir') ?></th>
              <td><?= h($album->image_dir) ?></td>
          </tr>
          <tr>
              <th scope="row"><?= __('Id') ?></th>
              <td><?= $this->Number->format($album->id) ?></td>
          </tr>
          <tr>
              <th scope="row"><?= __('Created By') ?></th>
              <td><?= $this->Number->format($album->created_by) ?></td>
          </tr>
          <tr>
              <th scope="row"><?= __('Modified By') ?></th>
              <td><?= $this->Number->format($album->modified_by) ?></td>
          </tr>
          <tr>
              <th scope="row"><?= __('Created') ?></th>
              <td><?= h($album->created) ?></td>
          </tr>
          <tr>
              <th scope="row"><?= __('Modified') ?></th>
              <td><?= h($album->modified) ?></td>
          </tr>
        </table>
    </div>
</div>

<div class="m-portlet  m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_1">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                  Pictures - <?= $album->title ?> Album
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="<?=$this->Url->build(['action'=>'index']);?>" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Back" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-left"></i></a>
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a>
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a>
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#" m-portlet-tool="remove" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-close"></i></a>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
      <div class="row">
        <?php foreach($album->pictures as $picture): ?>
          <div class="col-md-2">
            <img src="<?= substr($picture->path, 7).$picture->name ?>" class="img-fluid img-thumbnail" alt="<?= $picture->name ?>" title="<?= $picture->name ?>">
          </div>
        <?php endforeach; ?>
      </div>
    </div>
</div>
