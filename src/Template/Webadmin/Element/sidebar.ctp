    <!-- BEGIN: Left Aside -->
    <?php
        $url = $this->request->here();
      ?>
    <button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
    <div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
        <!-- BEGIN: Aside Menu -->
        <div
            id="m_ver_menu"
            class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
            m-menu-vertical="1"
            m-menu-scrollable="1" m-menu-dropdown-timeout="500"
            style="position: relative;">
            <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
               <li class="m-menu__section">
                    <h4 class="m-menu__section-text">
                        Accessibility
                    </h4>
                    <i class="m-menu__section-icon flaticon-more-v3"></i>
                </li>
                    <li class="m-menu__item <?= ($url == $this->Url->build(['controller'=>'Dashboard']) ? 'm-menu__item--active' : '' ) ?>" aria-haspopup="true" >
                        <a  href="<?= $this->Url->Build(['controller'=>'Dashboard','action'=>'index']) ?>" class="m-menu__link ">
                            <i class="m-menu__link-icon flaticon-line-graph"></i>
                            <span class="m-menu__link-title">
                                <span class="m-menu__link-wrap">
                                    <span class="m-menu__link-text">
                                        Dashboard
                                    </span>
                                </span>
                            </span>
                        </a>
                    </li>
                    <li class="m-menu__item <?= ($url == $this->Url->build(['controller'=>'Groups']) ? 'm-menu__item--active' : '' ) ?>" aria-haspopup="true" >
                        <a  href="<?= $this->Url->Build(['controller'=>'Groups','action'=>'index']) ?>" class="m-menu__link ">
                            <i class="m-menu__link-icon flaticon-users"></i>
                            <span class="m-menu__link-title">
                                <span class="m-menu__link-wrap">
                                    <span class="m-menu__link-text">
                                        Groups
                                    </span>
                                </span>
                            </span>
                        </a>
                    </li>



                    <li class="m-menu__item <?= ($url == $this->Url->build(['controller'=>'Users']) ? 'm-menu__item--active' : '' ) ?>" aria-haspopup="true" >
                        <a  href="<?= $this->Url->Build(['controller'=>'Users','action'=>'index']) ?>" class="m-menu__link ">
                            <i class="m-menu__link-icon flaticon-user-ok"></i>
                            <span class="m-menu__link-title">
                                <span class="m-menu__link-wrap">
                                    <span class="m-menu__link-text">
                                        Users
                                    </span>
                                </span>
                            </span>
                        </a>
                    </li>



                <!--  -->

                    <li class="m-menu__section">
                        <h4 class="m-menu__section-text">
                            Master
                        </h4>
                        <i class="m-menu__section-icon flaticon-more-v3"></i>
                    </li>

                    <li class="m-menu__item  m-menu__item--submenu <?= (@$_GET['content'] == 'index' || @$_GET['content'] == 'tentang' || @$_GET['content'] == 'career' || @$_GET['content'] == 'kontak' ? 'm-menu__item--open m-menu__item--expanded' : '') ?>" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="javascript:;" class="m-menu__link m-menu__toggle">
                            <i class="m-menu__link-icon flaticon-interface-6"></i>
                            <span class="m-menu__link-text">Content</span>
                            <i class="m-menu__ver-arrow la la-angle-right"></i>
                        </a>
                        <div class="m-menu__submenu ">
                            <span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                                    <span class="m-menu__link">
                                        <span class="m-menu__link-text">Content</span>
                                    </span>
                                </li>
                                <li class="m-menu__item <?= (@$_GET['content'] == 'index' ? 'm-menu__item--active' : '') ?>" aria-haspopup="true" >
                                    <a href="<?= $this->Url->Build(['controller'=>'Contents','action'=>'index', '?' => ['content' => 'index']]) ?>" class="m-menu__link ">
                                        <i class="m-menu__link-icon flaticon-line-graph"></i>
                                        <span class="m-menu__link-title">
                                            <span class="m-menu__link-wrap">
                                                <span class="m-menu__link-text">
                                                    Home
                                                </span>
                                            </span>
                                        </span>
                                    </a>
                                </li>


                                <li class="m-menu__item <?= (@$_GET['content'] == 'tentang' ? 'm-menu__item--active' : '') ?>" aria-haspopup="true" >
                                    <a href="<?= $this->Url->Build(['controller'=>'Contents','action'=>'index', '?' => ['content' => 'tentang']]) ?>" class="m-menu__link ">
                                        <i class="m-menu__link-icon flaticon-info"></i>
                                        <span class="m-menu__link-title">
                                            <span class="m-menu__link-wrap">
                                                <span class="m-menu__link-text">
                                                    About
                                                </span>
                                            </span>
                                        </span>
                                    </a>
                                </li>

                                <li class="m-menu__item <?= (@$_GET['content'] == 'career' ? 'm-menu__item--active' : '') ?>" aria-haspopup="true" >
                                    <a href="<?= $this->Url->Build(['controller'=>'Contents','action'=>'index', '?' => ['content' => 'career']]) ?>" class="m-menu__link ">
                                        <i class="m-menu__link-icon flaticon-folder-1"></i>
                                        <span class="m-menu__link-title">
                                            <span class="m-menu__link-wrap">
                                                <span class="m-menu__link-text">
                                                    Career
                                                </span>
                                            </span>
                                        </span>
                                    </a>
                                </li>


                                <li class="m-menu__item <?= (@$_GET['content'] == 'kontak' ? 'm-menu__item--active' : '') ?>" aria-haspopup="true" >
                                    <a href="<?= $this->Url->Build(['controller'=>'Contents','action'=>'index', '?' => ['content' => 'kontak']]) ?>" class="m-menu__link ">
                                        <i class="m-menu__link-icon flaticon-computer"></i>
                                        <span class="m-menu__link-title">
                                            <span class="m-menu__link-wrap">
                                                <span class="m-menu__link-text">
                                                    Contact
                                                </span>
                                            </span>
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li class="m-menu__item <?= ($url == $this->Url->build(['controller'=>'Covers']) ? 'm-menu__item--active' : '' ) ?>" aria-haspopup="true" >
                        <a href="<?= $this->Url->Build(['controller'=>'Covers','action'=>'index']) ?>" class="m-menu__link ">
                            <i class="m-menu__link-icon flaticon-tabs"></i>
                            <span class="m-menu__link-title">
                                <span class="m-menu__link-wrap">
                                    <span class="m-menu__link-text">
                                        Banner
                                    </span>
                                </span>
                            </span>
                        </a>
                    </li>
                    <li class="m-menu__item  m-menu__item--submenu <?= ($url == $this->Url->build(['controller'=>'Products']) || $url == $this->Url->build(['controller'=>'Pictures','?'=>['id'=>'product']]) || $url == $this->Url->build(['controller'=>'Documents']) ? 'm-menu__item--open m-menu__item--active' : '' ) ?>" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="javascript:;" class="m-menu__link m-menu__toggle ">
                            <i class="m-menu__link-icon flaticon-interface-6"></i>
                            <span class="m-menu__link-text">Product</span>
                            <i class="m-menu__ver-arrow la la-angle-right"></i>
                        </a>
                        <div class="m-menu__submenu ">
                            <span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                                    <span class="m-menu__link">
                                        <span class="m-menu__link-text">Product</span>
                                    </span>
                                </li>
                                <li class="m-menu__item <?= ($url == $this->Url->build(['controller'=>'Products']) ? 'm-menu__item--active' : '' ) ?>" aria-haspopup="true" >
                                    <a href="<?= $this->Url->Build(['controller'=>'Products','action'=>'index']) ?>" class="m-menu__link ">
                                        <i class="m-menu__link-icon flaticon-business"></i>
                                        <span class="m-menu__link-title">
                                            <span class="m-menu__link-wrap">
                                                <span class="m-menu__link-text">
                                                    Product
                                                </span>
                                            </span>
                                        </span>
                                    </a>
                                </li>

                                <li class="m-menu__item <?= ($url == $this->Url->build(['controller'=>'Pictures','?'=>['id'=>'product']]) ? 'm-menu__item--active' : '' ) ?>" aria-haspopup="true" >
                                    <a href="<?= $this->Url->Build(['controller'=>'Pictures','action'=>'index','?'=>['id'=>'product']]) ?>" class="m-menu__link ">
                                        <i class="m-menu__link-icon flaticon-add"></i>
                                        <span class="m-menu__link-title">
                                            <span class="m-menu__link-wrap">
                                                <span class="m-menu__link-text">
                                                    Pictures  <br>
                                                </span>
                                            </span>
                                        </span>
                                    </a>
                                </li>

                                <li class="m-menu__item <?= ($url == $this->Url->build(['controller'=>'Documents']) ? 'm-menu__item--active' : '' ) ?>" aria-haspopup="true" >
                                    <a href="<?= $this->Url->Build(['controller'=>'Documents','action'=>'index']) ?>" class="m-menu__link ">
                                        <i class="m-menu__link-icon flaticon-file-1"></i>
                                        <span class="m-menu__link-title">
                                            <span class="m-menu__link-wrap">
                                                <span class="m-menu__link-text">
                                                    Documents  <br>
                                                </span>
                                            </span>
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>


                    <li class="m-menu__item  m-menu__item--submenu <?= ( $url == $this->Url->build(['controller'=>'Albums']) || $url == $this->Url->build(['controller'=>'Pictures','?'=>['id'=>'album']]) ? 'm-menu__item--open m-menu__item--active' : '' ) ?>" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="javascript:;" class="m-menu__link m-menu__toggle">
                            <i class="m-menu__link-icon flaticon-interface-6"></i>
                            <span class="m-menu__link-text">Gallery</span>
                            <i class="m-menu__ver-arrow la la-angle-right"></i>
                        </a>
                        <div class="m-menu__submenu ">
                            <span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                                    <span class="m-menu__link">
                                        <span class="m-menu__link-text">Gallery</span>
                                    </span>
                                </li>
                                <li class="m-menu__item <?= ($url == $this->Url->build(['controller'=>'Albums']) ? 'm-menu__item--active' : '' ) ?>" aria-haspopup="true" >
                                    <a href="<?= $this->Url->Build(['controller'=>'Albums','action'=>'index']) ?>" class="m-menu__link ">
                                        <i class="m-menu__link-icon flaticon-web"></i>
                                        <span class="m-menu__link-title">
                                            <span class="m-menu__link-wrap">
                                                <span class="m-menu__link-text">
                                                    Albums
                                                </span>
                                            </span>
                                        </span>
                                    </a>
                                </li>

                                <li class="m-menu__item <?= ($url == $this->Url->build(['controller'=>'Pictures']) ? 'm-menu__item--active' : '' ) ?>" aria-haspopup="true" >
                                <li class="m-menu__item <?= ($url == $this->Url->build(['controller'=>'Pictures','?'=>['id'=>'album']]) ? 'm-menu__item--active' : '' ) ?>" aria-haspopup="true" >
                                    <a href="<?= $this->Url->Build(['controller'=>'Pictures','action'=>'index','?'=>['id'=>'album']]) ?>" class="m-menu__link ">
                                        <i class="m-menu__link-icon flaticon-add"></i>
                                        <span class="m-menu__link-title">
                                            <span class="m-menu__link-wrap">
                                                <span class="m-menu__link-text">
                                                    Pictures
                                                </span>
                                            </span>
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="m-menu__item <?= ($url == $this->Url->build(['controller'=>'Recruters']) ? 'm-menu__item--active' : '' ) ?>" aria-haspopup="true" >
                        <a href="<?= $this->Url->Build(['controller'=>'Recruters','action'=>'index']) ?>" class="m-menu__link ">
                            <i class="m-menu__link-icon flaticon-users"></i>
                            <span class="m-menu__link-title">
                                <span class="m-menu__link-wrap">
                                    <span class="m-menu__link-text">
                                        Recruters
                                    </span>
                                </span>
                            </span>
                        </a>
                    </li>



            </ul>
        </div>
        <!-- END: Aside Menu -->
    </div>
    <!-- END: Left Aside -->
