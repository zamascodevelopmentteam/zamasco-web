<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Picture $picture
 */
?>

<div class="m-portlet  m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_1">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                <?=$titlesubModule;?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="<?=$this->Url->build(['action'=>'index','?'=>['id'=>$url[1]]]);?>" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Back" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-left"></i></a>
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a>
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a>
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#" m-portlet-tool="remove" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-close"></i></a>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <table class="table">
          <tr>
              <th scope="row"><?= __('Name') ?></th>
              <td><?= h($picture->name) ?></td>
          </tr>
          <tr>
              <th scope="row"><?= __('Path') ?></th>
              <td><?= h($picture->path) ?></td>
          </tr>
          <?php  if($url[1] == 'product') : ?>
              <tr>
                <th scope="row"><?= __('Product') ?></th>
                <td><?= $picture->has('product') ? $this->Html->link($picture->product->name, ['controller' => 'Products', 'action' => 'view', $picture->product->id]) : '' ?></td>
            </tr>
          <?php elseif($url[1] == 'album'): ?>
            <tr>
                <th scope="row"><?= __('album') ?></th>
                <td><?= $picture->has('album') ? $this->Html->link($picture->album->title, ['controller' => 'Albums', 'action' => 'view', $picture->album->id]) : '' ?></td>
            </tr>
          <?php endif; ?>
          <tr>
              <th scope="row"><?= __('Id') ?></th>
              <td><?= $this->Number->format($picture->id) ?></td>
          </tr>
          <tr>
              <th scope="row"><?= __('Created By') ?></th>
              <td><?= $this->Number->format($picture->created_by) ?></td>
          </tr>
          <tr>
              <th scope="row"><?= __('Modified By') ?></th>
              <td><?= $this->Number->format($picture->modified_by) ?></td>
          </tr>
          <tr>
              <th scope="row"><?= __('Created') ?></th>
              <td><?= h($picture->created) ?></td>
          </tr>
          <tr>
              <th scope="row"><?= __('Modified') ?></th>
              <td><?= h($picture->modified) ?></td>
          </tr>
        </table>
    </div>
</div>
