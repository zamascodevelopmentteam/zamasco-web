<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Picture[]|\Cake\Collection\CollectionInterface $pictures
 */
?>


<?php
    $rightButton = "";
    if($this->Acl->check(['action'=>'add']) == true):
        $rightButton = '<a href="'.$this->Url->build(['action'=>'add','?'=>['id'=>$url[1]]]).'" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                            <span>
                                <i class="la la-plus"></i>
                                <span>
                                    Tambah Data
                                </span>
                            </span>
                        </a>';
    endif;
?>
<?=$this->element('widget/index',['rightButton' => $rightButton]);?>
<?php $this->start('script');?>
    <script>
        <?php
            $deleteUrl    = $this->Url->build(['action'=>'delete'])."/";
            if($this->Acl->check(['action'=>'delete']) == false){
                $deleteUrl = "";
            }
            $editUrl      = $this->Url->build(['action'=>'edit'])."/";
            if($this->Acl->check(['action'=>'edit']) == false){
                $editUrl = "";
            }
            $viewUrl = $this->Url->build(['action'=>'view'])."/";
            if($this->Acl->check(['action'=>'view']) == false){
                $viewUrl = "";
            }
        ?>
        jQuery(document).ready(function() {
            var deleteUrl = "<?=$deleteUrl;?>";
            var editUrl = "<?=$editUrl;?>";
            var viewUrl = "<?=$viewUrl;?>";
            var columnData = [{
                field: "Pictures.id",
                title: "ID",
                sortable: false,
                width: 40,
                selector: false,
                textAlign: "center",
                template: function(t) {
                    return t.id
                }
            },  {
                field: "Pictures.path",
                title: "Path",
                sort : 'asc',
                template: function(t) {
                    return "<img src='" + t.path.substring(7) + t.name + "' width='150' >"
                }
            },  {
                field: "Albums.<?= $url[1] ?>",
                title: "<?= ucfirst($url[1]) ?>",
                sort : 'asc',
                template: function(t) {
                    if('<?= $url[1] ?>' == 'product'){
                        return t.product.name   
                    }else if('<?= $url[1] ?>' == 'album'){
                        return t.album.title
                    }
                    
                }
            },  {
                field: "Pictures.created",
                title: "Created",
                sort : 'asc',
                template: function(t) {
                    return Utils.dateIndonesia(t.created,true,true)
                }
            },
            {
                field: "actions",
                width: 150,
                title: "Actions",
                sortable: false,
                overflow: "visible",
                template: function(t) {
                    var btnList = '';
                    if(viewUrl != ""){
                        btnList += '<a class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" href="'+viewUrl+t.id+'?id=<?= $url[1] ?>" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="View"><i class="flaticon-search-1"></i></a>';
                    }
                    if(editUrl != ""){
                        btnList += '<a class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" href="'+editUrl+t.id+'?id=<?= $url[1] ?>" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Edit"><i class="flaticon-edit"></i></a>';
                    }
                    if(deleteUrl != ""){
                        btnList += '<a class="btn-delete-on-table m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" href="'+deleteUrl+t.id+'" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Delete"><i class="flaticon-cancel"></i></a>';
                    }
                    return btnList;
                }
            }];
            DatatableRemoteAjaxDemo.init("",columnData,"<?=$this->request->getParam('_csrfToken');?>")
        });

$(document).ready(function(){
        $('#m_form_search').val() == "home";
    });
    </script>
<?php $this->end();?>
