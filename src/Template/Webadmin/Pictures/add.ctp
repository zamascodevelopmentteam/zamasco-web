<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Picture $picture
 */
?>


<div class="m-portlet  m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_1">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                <?=$titlesubModule;?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="<?=$this->Url->build(['action'=>'index','?'=>['id'=>'product']]);?>" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Back" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-left"></i></a>
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a>
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a>
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#" m-portlet-tool="remove" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-close"></i></a>
                </li>
            </ul>
        </div>
    </div>
    <?= $this->Form->create($picture,['class'=>'m-form m-form--fit m-form--label-align-right','type'=>'file']) ?>
        <div class="m-portlet__body">
            <?php for ($i=0; $i < 1; $i++) {?>
            <div class="row m--margin-bottom-15">
                <div class="col-md-4">
                    <?=$this->Form->control($i.'.name',['type'=>'file','required']);?>
                </div>
                <div class="col-md-4 <?= $url[1] ?>" <?= ($i == 0 ? "" : "style='visibility: hidden;'" )?>>
                    
                      <?= $this->Form->control($i.'.'.$url[1].'_id', ['empty' => 'Choose','label'=> ucfirst($url[1]), 'options'=>$pages,'required']) ?>
                    
                </div>
                <div class="col-md-4 title" style="visibility: hidden;">
                    <?= $this->Form->control($i.'.title') ?>
                </div>
            </div>
            <?php } ?>


        </div>


        <div class="m-form__actions m-form__actions--sm m-form__actions--solid">
            <button type="submit" class="btn btn-primary">
                Submit
            </button>
            <button type="reset" class="btn btn-secondary">
                Cancel
            </button>
        </div>
    <?= $this->Form->end();?>
</div>
<script type="text/javascript">
  var title;
  for (var i = 0; i < 10; i++) {
      title = $('#'+i+'-<?= $url[1] ?>-id').find(':selected').text();
      $('#'+i+'-title').val(title);
      

  }
  $('#0-<?= $url[1] ?>-id').change(function() {
  title = $(this).find(':selected').text();
    $('.title .m-input').val(title);
  title = $(this).find(':selected').val();
    $('.<?= $url[1] ?> .m-input').val(title);
  });
  $('#1-<?= $url[1] ?>-id').change(function() {
  title = $('#1-<?= $url[1] ?>-id').find(':selected').text();
    $('#1-title').val(title);
  });
  $('#2-<?= $url[1] ?>-id').change(function() {
  title = $('#2-<?= $url[1] ?>-id').find(':selected').text();
    $('#2-title').val(title);
  });
  $('#3-<?= $url[1] ?>-id').change(function() {
  title = $('#3-<?= $url[1] ?>-id').find(':selected').text();
    $('#3-title').val(title);
  });
  $('#4-<?= $url[1] ?>-id').change(function() {
  title = $('#4-<?= $url[1] ?>-id').find(':selected').text();
    $('#4-title').val(title);
  });
  $('#5-<?= $url[1] ?>-id').change(function() {
  title = $('#5-<?= $url[1] ?>-id').find(':selected').text();
    $('#5-title').val(title);
  });
  $('#6-<?= $url[1] ?>-id').change(function() {
  title = $('#6-<?= $url[1] ?>-id').find(':selected').text();
    $('#6-title').val(title);
  });
  $('#7-<?= $url[1] ?>-id').change(function() {
  title = $('#7-<?= $url[1] ?>-id').find(':selected').text();
    $('#7-title').val(title);
  });
  $('#8-<?= $url[1] ?>-id').change(function() {
  title = $('#8-<?= $url[1] ?>-id').find(':selected').text();
    $('#8-title').val(title);
  });
  $('#9-<?= $url[1] ?>-id').change(function() {
  title = $('#9-<?= $url[1] ?>-id').find(':selected').text();
    $('#9-title').val(title);
  });
</script>
