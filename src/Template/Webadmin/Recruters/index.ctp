<?php
    $rightButton = "";
    if($this->Acl->check(['action'=>'add']) == true):
        $rightButton = '<a href="'.$this->Url->build(['action'=>'add']).'" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                            <span>
                                <i class="la la-plus"></i>
                                <span>
                                    Tambah Data
                                </span>
                            </span>
                        </a>';
    endif;
?>
<?=$this->element('widget/index',['rightButton' => $rightButton]);?>
<?php $this->start('script');?>
    <script>
        <?php
            $viewUrl = $this->Url->build(['action'=>'view'])."/";
            if($this->Acl->check(['action'=>'view']) == false){
                $viewUrl = "";
            }
        ?>
        jQuery(document).ready(function() {
            var viewUrl = "<?=$viewUrl;?>";
            var columnData = [{
                field: "Recruters.id",
                title: "ID",
                sortable: false,
                width: 40,
                selector: false,
                textAlign: "center",
                template: function(t) {
                    return t.id
                }
            },
            {
                field: "Recruters.job_title",
                title: "Job Title",
                sort : 'asc',
                template: function(t) {
                    return t.job_title
                }
            },
            {
                field: "Recruters.name",
                title: "Name",
                sort : 'asc',
                template: function(t) {
                    return t.name
                }
            },
            {
                field: "Recruters.created",
                title: "Created",
                sort : 'asc',
                template: function(t) {
                    return Utils.dateIndonesia(t.created,true,true)
                }
            },
            {
                field: "actions",
                width: 150,
                title: "Actions",
                sortable: false,
                overflow: "visible",
                template: function(t) {
                    var btnList = '';
                    if(viewUrl != ""){
                        btnList += '<a class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" href="'+viewUrl+t.id+'" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="View"><i class="flaticon-search-1"></i></a>';
                    }
                    return btnList;
                }
            }];
            DatatableRemoteAjaxDemo.init("",columnData,"<?=$this->request->getParam('_csrfToken');?>")
        });
    </script>
<?php $this->end();?>
