<div class="m-portlet  m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_1">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?=$titlesubModule;?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="<?=$this->Url->build(['action'=>'index']);?>" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Back" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-left"></i></a>
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a>
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a>
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#" m-portlet-tool="remove" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-close"></i></a>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <table class="table">
            <tr>
                <th scope="row" width="20%"><?= __('Job Title') ?></th>
                <td><?= h($recruter->job_title) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Name') ?></th>
                <td><?= h($recruter->name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Phone') ?></th>
                <td><?= h($recruter->phone) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Address') ?></th>
                <td><?= h($recruter->address) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Education') ?></th>
                <td><?= h($recruter->education) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('File') ?></th>
                <td>
                    <a href="<?= $this->Utilities->generateUrlImage($recruter->file_dir, $recruter->file) ?>" class="btn btn-primary btn-sm" target="_blank">
                        <i class="fa fa-download"></i> Download
                    </a>
                </td>
            </tr>
            <tr>
                <th scope="row"><?= __('Created') ?></th>
                <td><?= h($recruter->created->nice()) ?></td>
            </tr>
        </table>
    </div>
</div>