<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Content $content
 */
?>

<div class="m-portlet  m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_1">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                <?=$titlesubModule;?>
                </h3>
            </div>          
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">                     
                <li class="m-portlet__nav-item">
                    <a href="<?=$this->Url->build(['action'=>'index']);?>" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Back" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-left"></i></a> 
                </li>                       
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a> 
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a> 
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#" m-portlet-tool="remove" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-close"></i></a>   
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <table class="table">
            <tr>
                <th scope="row"><?= __('Title') ?></th>
                <td><?= $cover->title ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Title') ?></th>
                <td><?= $cover->text_content ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Pages') ?></th>
                <td><?= h($cover->pages) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Picture') ?></th>
                <td>
                    <!-- <?= h($cover->picture) ?> -->
                    <img src="<?= substr(h($cover->picture_dir), 7).h($cover->picture) ?>" alt="" width="300">
                </td>
            </tr>
            <tr>
                <th scope="row"><?= __('Picture Dir') ?></th>
                <td><?= h($cover->picture_dir).h($cover->picture) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($cover->id) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Status') ?></th>
                <td><?= $this->Utilities->statusLabel($cover->status) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Created By') ?></th>
                <td><?= $this->Number->format($cover->created_by) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modified By') ?></th>
                <td><?= $this->Number->format($cover->modified_by) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Created') ?></th>
                <td><?= h($cover->created) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modified') ?></th>
                <td><?= h($cover->modified) ?></td>
            </tr>
        </table>
    </div>
</div>







