<div class="m-portlet  m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_1">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                <?=$titlesubModule;?>
                </h3>
            </div>			
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">							
                <li class="m-portlet__nav-item">
                    <a href="<?=$this->Url->build(['action'=>'index']);?>" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Back" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-left"></i></a>	
                </li>									
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a>	
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a>	
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#" m-portlet-tool="remove" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-close"></i></a>	
                </li>
            </ul>
        </div>
    </div>
    <?= $this->Form->create($dataSave,['class'=>'m-form m-form--fit m-form--label-align-right']) ?>
        <div class="m-portlet__body">
        <div class="row m--margin-bottom-15">
                <div class="col-md-6">
                    <?=$this->Form->control('App_Name',[
                        'label'=>'App Name',
                        'value' => $appSettings['App_Name']->valueField
                    ]);;?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-3">
                    <?=$this->Form->control('App_Logo_Width',[
                        'label'=>'App Logo Width',
                        'value' => $appSettings['App_Logo_Width']->valueField
                    ]);;?>
                </div>
                <div class="col-md-3">
                    <?=$this->Form->control('App_Logo_Height',[
                        'label'=>'App Logo Height',
                        'value' => $appSettings['App_Logo_Height']->valueField
                    ]);;?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('App_Logo',[
                        'label'=>'App Logo',
                        'class' => 'form-control m-input',
                        'type' => 'file'
                    ]);;?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-3">
                    <?=$this->Form->control('App_Logo_Login_Width',[
                        'label'=>'App Logo Width',
                        'value' => $appSettings['App_Logo_Login_Width']->valueField
                    ]);;?>
                </div>
                <div class="col-md-3">
                    <?=$this->Form->control('App_Logo_Login_Height',[
                        'label'=>'App Logo Login Height',
                        'value' => $appSettings['App_Logo_Login_Height']->valueField
                    ]);;?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('App_Logo_Login',[
                        'label'=>'App Logo Login',
                        'class' => 'form-control m-input',
                        'type' => 'file'
                    ]);;?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-4">
                    <?=$this->Form->control('App_Login_Cover',[
                        'label'=>'App Login Cover',
                        'class' => 'form-control m-input',
                        'type' => 'file'
                    ]);;?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-4">
                    <?=$this->Form->control('App_Favico',[
                        'label'=>'App Favico',
                        'class' => 'form-control m-input',
                        'type' => 'file'
                    ]);;?>
                </div>
            </div>
        </div>
        <div class="m-form__actions m-form__actions--sm m-form__actions--solid">
            <button type="submit" class="btn btn-primary">
                Submit
            </button>
            <button type="reset" class="btn btn-secondary">
                Cancel
            </button>
        </div>
    <?= $this->Form->end();?>
</div>
