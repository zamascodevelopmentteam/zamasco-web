<div class="m-portlet  m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_1">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                <?=$titlesubModule;?>
                </h3>
            </div>			
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">							
                <li class="m-portlet__nav-item">
                    <a href="<?=$this->Url->build(['action'=>'index']);?>" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Back" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-left"></i></a>	
                </li>									
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a>	
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a>	
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#" m-portlet-tool="remove" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-close"></i></a>	
                </li>
            </ul>
        </div>
    </div>
    <?= $this->Form->create($user,['class'=>'m-form m-form--fit m-form--label-align-right']) ?>
        <div class="m-portlet__body">
            <table class="table m-table">
                <thead>
                    <tr>
                        <th width="30px"><input type="checkbox" id="allCheck"></th>
                        <th>Module Name</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($acos as $key => $aco):?>
                        <tr class="m-table__row--primary">
                            <td>
                                <?=$this->Form->checkbox('aco_parent_id.'.$aco->alias,[
                                    'checked' => ($aco->read  == 1 ? true:false),
                                    'value' => 1,
                                    'class' => 'checkParent',
                                    'data-id' => $aco->id,
                                ]);?>
                            </td>
                            <td><?=$aco->name;?></td>
                        </tr>
                        <?php foreach($aco->children as $ckey => $child):?>
                            <tr class="">
                                <td>
                                    <?=$this->Form->checkbox('aco_id.'.$aco->alias.'.'.$child->alias,[
                                        'checked' => ($child->read  == 1 ? true:false),
                                        'value' => 1,
                                        'class' => 'checkChild',
                                        'data-parent' => $aco->id,
                                    ]);?>
                                </td>
                                <td><?=$child->name;?></td>
                            </tr>
                        <?php endforeach;?>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
        <div class="m-form__actions m-form__actions--sm m-form__actions--solid">
            <button type="submit" class="btn btn-primary">
                Submit
            </button>
            <button type="reset" class="btn btn-secondary">
                Cancel
            </button>
        </div>
    <?= $this->Form->end();?>
</div>

<?=$this->Html->scriptBlock(
    '
        $("#allCheck").on("click",function(e){
            if($(this).prop("checked")){
                $("input[type=\"checkbox\"]").prop("checked",true);
            }else{
                $("input[type=\"checkbox\"]").prop("checked",false);
            }
        })
        $(".checkParent").on("click",function(e){
            console.log($(this).val())
            if($(this).prop("checked")){
                $("input[data-parent=\""+$(this).data("id")+"\"]").prop("checked",true);
            }else{
                $("input[data-parent=\""+$(this).data("id")+"\"]").prop("checked",false);
            }
        })
        $(".checkChild").on("click",function(e){
            var thisParentVal = $(this).data("parent");
            console.log(thisParentVal);
            if($(this).prop("checked")){
                $("input[data-id=\""+thisParentVal+"\"]").prop("checked",true);
            }else{
                var countCheck = $("input[data-parent=\""+thisParentVal+"\"]:checked").length;
                if(countCheck == 0){
                    $("input[data-id=\""+thisParentVal+"\"]").prop("checked",false);
                }
                //$("input[data-parent=\""+$(this).val()+"\"]").prop("checked",false);
            }
        })
    ',[
        'block'=>'script'
    ]
);?>