<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Content $content
 */
?>

<div class="m-portlet  m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_1">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= $titlesubModule; ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="<?= $this->Url->build(['action' => 'index', '?' => ['content' => $_GET['content']]]); ?>" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Back" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-left"></i></a>
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#" m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a>
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#" m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a>
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#" m-portlet-tool="remove" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-close"></i></a>
                </li>
            </ul>
        </div>
    </div>
    <?= $this->Form->create($content, ['class' => 'm-form m-form--fit m-form--label-align-right', 'type' => 'file']) ?>
    <div class="m-portlet__body">
        <div class="row m--margin-bottom-15">
            <div class="col-md-4">
                <?= $this->Form->control('page', ['type' => 'select', 'required' => 'required', 'options' => ['index' => 'Home', 'tentang' => 'About', 'career' => 'Career', 'kontak' => 'Contact']]); ?>
            </div>
            <div class="col-md-4">
                <?= $this->Form->control('position', ['type' => 'select']); ?>
            </div>
            <div class="col-md-4">
                <?= $this->Form->control('link') ?>
            </div>
        </div>
        <div class="row m--margin-bottom-15">
            <div class="col-md-4">
                <?= $this->Form->control('picture', ['class' => 'm-input form-control', 'type' => 'file', 'id' => 'pic', 'label' => 'Picture / File']); ?>
                <img src="<?= substr($content->picture_dir, 7) . $content->picture ?>" alt="" class="img-fluid" style="border-radius: 2px;box-shadow: 5px 6px 20px #ddd">
            </div>
            <div class="col-md-4">
                <?= $this->Form->control('status', ['options' => ['DISABLED', 'ENABLED'], 'default' => 0]); ?>
            </div>
        </div>
        <div class="row m--margin-bottom-15">
            <div class="col-md-4">
                <?= $this->Form->control('title'); ?>
            </div>
            <div class="col-md-4">
                <?= $this->Form->control('text_content', ['type' => 'textarea', 'class' => 'form-control', 'label' => '*Text Content']); ?>
            </div>
        </div>
    </div>


    <div class="m-form__actions m-form__actions--sm m-form__actions--solid">
        <button type="submit" class="btn btn-primary">
            Submit
        </button>
        <button type="reset" class="btn btn-secondary">
            Cancel
        </button>
    </div>
    <?= $this->Form->end(); ?>
</div>

<?php $this->start('script') ?>

<script>
    $('#page').on('change', function() {
        var INDEX = {
            '': '',
            'consultant': 'Consultant Division',
            'medical-device': 'Medical Device Division',
            'technology': 'Technology Division',
            'video': 'Video',
            'slogan': 'Judul Footer',
            'footer1': 'Logo Klien'
        };

        var TENTANG = {
            '': '',
            'intro': 'Intro',
            'visi': 'Visi',
            'misi': 'Misi',
            'technology': 'Divisi Tech',
            'ehealth': 'Divisi E-Health',
            'consultant': 'Divisi Consultant',
            'moto': 'Title Moto',
            'moto1': 'Moto',
            'komisaris': 'Commissioner',
            'anggota': 'Foto Director / Manager',
            'foteam': 'Foto Team',
            'struktur': 'Struktur',
            'logo-visi-misi': 'Logo Visi Misi'
        };

        var CAREER = {
            '': '',
            'career': 'LoKer'
        };

        var KONTAK = {
            '': '',
            'video': 'Video',
            'alamat': 'Alamat',
            'tlpn1': 'Telephone Kantor',
            'tlpn2': 'Telephone Seluler',
            'email': 'email',
            'intro': 'Intro Zamasco'
        };

        $("#position").change(function() {
            if ($(this).val() == 'career') {
                $(".pic label").text("*Picture (1000 x 1000)");
            }
        });

        var select = $('#position');
        if (select.prop) {
            var options = select.prop('options');
        } else {
            var options = select.attr('options');
        }
        $('option', select).remove();

        if (this.value == "index") {
            $.each(INDEX, function(val, text) {
                options[options.length] = new Option(text, val);
            });
        } else if (this.value == "tentang") {
            $.each(TENTANG, function(val, text) {
                options[options.length] = new Option(text, val);
            });
        } else if (this.value == "kontak") {
            $.each(KONTAK, function(val, text) {
                options[options.length] = new Option(text, val);
            });
        } else if (this.value == "career") {
            $.each(CAREER, function(val, text) {
                options[options.length] = new Option(text, val);
            });
        }



    });

    $('#page').trigger('change');
    $('#position').val('<?= $content->position ?>');
</script>

<?php $this->end() ?>