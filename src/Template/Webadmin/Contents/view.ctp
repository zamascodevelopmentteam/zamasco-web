<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Content $content
 */
?>

<div class="m-portlet  m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_1">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                <?=$titlesubModule;?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="<?=$this->Url->build(['action'=>'index', '?' => ['content' => $_GET['content']]]);?>" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Back" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-left"></i></a>
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a>
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a>
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#" m-portlet-tool="remove" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-close"></i></a>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <table class="table">
            <tr>
                <th scope="row"><?= __('Page') ?></th>
                <td><?= h($content->page) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Position') ?></th>
                <td><?= h($content->position) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Title') ?></th>
                <td><?= h($content->title) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Text Content') ?></th>
                <td><?= h($content->text_content) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($content->id) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Status') ?></th>
                <td><?= $this->Utilities->statusLabel($content->status) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Created By') ?></th>
                <td><?= $this->Number->format($content->created_by) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modified By') ?></th>
                <td><?= $this->Number->format($content->modified_by) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Created') ?></th>
                <td><?= h($content->created) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modified') ?></th>
                <td><?= h($content->modified) ?></td>
            </tr>
        </table>
    </div>
</div>
