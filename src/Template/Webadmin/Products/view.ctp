<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Product $product
 */
?>


<div class="m-portlet  m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_1">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                <?=$titlesubModule;?>
                </h3>
            </div>          
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">                     
                <li class="m-portlet__nav-item">
                    <a href="<?=$this->Url->build(['action'=>'index']);?>" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Back" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-left"></i></a> 
                </li>                       
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a> 
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a> 
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#" m-portlet-tool="remove" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-close"></i></a>   
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <table class="table">
            <tr>
                <th scope="row"><?= __('Name') ?></th>
                <td><?= $product->name ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Picture') ?></th>
                <td><?= $product->picture ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Picture Dir') ?></th>
                <td><?= $product->picture_dir ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Links') ?></th>
                <td><?= $product->links ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Division') ?></th>
                <td><?= $product->division ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Category') ?></th>
                <td><?= $product->category ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($product->id) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Status') ?></th>
                <td><?= $this->Utilities->statusLabel($product->status) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Created By') ?></th>
                <td><?= $this->Number->format($product->created_by) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modified By') ?></th>
                <td><?= $this->Number->format($product->modified_by) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Created') ?></th>
                <td><?= $product->created ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modified') ?></th>
                <td><?= $product->modified ?></td>
            </tr>
        </table>
    </div>
</div>

<div class="m-portlet  m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_1">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                Preview
                </h3>
            </div>          
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">                     
                <li class="m-portlet__nav-item">
                    <a href="<?=$this->Url->build(['action'=>'index']);?>" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Back" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-left"></i></a> 
                </li>                       
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a> 
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a> 
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#" m-portlet-tool="remove" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-close"></i></a>   
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="section-heading-left">
                        <h2><?= strtoupper(h($product->name)) ?></h2>
                    </div>
                    <br>
                    <div class="content">
                        <?= $product->content ?>
                    </div>
                    <br>
                    <a href="" class="btn btn-primary pull-right">Live Demo</a>
                </div>
                <div class="col-md-6">
                    <img src="<?= substr(h($product->picture_dir), 7).h($product->picture) ?>" alt="" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css" media="screen">
    .section-heading h2, .section-heading-left h2 {
    font-size: 33px;
    font-weight: 300;
    color: #3b3b3b;
    line-height: 130%}
    .section-heading h2:after, .section-heading-left h2:after {
    padding-top: 15px;
    content: '';
    display: block;
    border-bottom: 2px solid #2578ff;
    width: 100px;
}
.section-heading {
    margin-bottom: 70px;
}
.section-heading h2 {
    margin-bottom: 10px;
}
.section-heading h2:after {
    margin: auto;
}
.section-heading-left h2 {
    margin-bottom: 10px;
}
.section-heading-left p {
    font-size: 15px;
    font-weight: 500;
    color: #979797;
    line-height: 160%;
    padding-top: 10px;
    margin-bottom: 0;
}
.content p{
    font-size: 20px;
}
.content li{
    font-size: 15px;
}
</style>

