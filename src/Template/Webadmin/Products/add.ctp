<div class="m-portlet  m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_1">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= $titlesubModule; ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="<?= $this->Url->build(['action' => 'index']); ?>" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Back" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-left"></i></a>
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#" m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a>
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#" m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a>
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#" m-portlet-tool="remove" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-close"></i></a>
                </li>
            </ul>
        </div>
    </div>
    <?= $this->Form->create($product, ['class' => 'm-form m-form--fit m-form--label-align-right', 'type' => 'file']) ?>
    <div class="m-portlet__body">
        <div class="row">
            <div class="col-md-8">
                <div class="row m--margin-bottom-15">
                    <div class="col-md-6">
                        <?= $this->Form->control('name'); ?>
                    </div>
                    <div class="col-md-6">
                        <?= $this->Form->control('picture', ['class' => 'm-input form-control', 'type' => 'file', 'label' => '*Picture', 'required']); ?>
                    </div>
                </div>
                <div class="row m--margin-bottom-15">
                    <div class="col-md-6">
                        <?= $this->Form->control('links'); ?>
                    </div>
                    <div class="col-md-6">
                        <?= $this->Form->control('status', ['options' => ['DISABLED', 'ENABLED'], 'default' => 1]); ?>
                    </div>
                </div>

                <div class="row m--margin-bottom-15">

                    <div class="col-md-12">
                        <?= $this->Form->control('content', ['type' => 'textarea', 'required' => false]); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row m--margin-bottom-15">
                    <div class="col-md-12">
                        <?= $this->Form->control('division', ['type' => 'select', 'options' => [
                            'it' => 'Technology',
                            'website' => 'Website',
                            'medical-device' => 'Medical Device',
                            'catalog' => 'E Catalog',
                            'consultant' => 'Consultant Management',
                        ]]) ?>
                    </div>
                </div>
                <div class="row m--margin-bottom-15">
                    <div class="col-md-12">

                        <?= $this->Form->control('category', ['type' => 'select']); ?>
                    </div>
                </div>
            </div>
        </div>


    </div>


    <div class="m-form__actions m-form__actions--sm m-form__actions--solid">
        <button type="submit" class="btn btn-primary" id="submit">
            Submit
        </button>
        <button type="reset" class="btn btn-secondary">
            Cancel
        </button>
    </div>

    <?= $this->Form->end(); ?>
</div>
<script>
    $('#division').append('<option selected disabled first></option>');
    // get
    $('#division').on('change', function() {
        // this.value
        // set
        var IT = {
            '': '',
            // 'web' : 'Web',
            'mobile': 'Mobile',
            'software': 'Software',
            'hardware': 'Hardware',
            'service': 'Service'
        };

        var WEBSITE = {
            '': '',
            'company-profile': 'Company Profile',
            'organization': 'Organization',
            'personal-website': 'Personal Website',
            'wedding': 'Wedding'
        };

        var MEDICAL_DEVICE = {
            '': '',
            'rumahsakit': 'Medical Disposible',
            'survey': 'Medical Equipment',
            'diagnostic': 'Laboratorium Diagnostic',
            'telemedicine': 'Telemedicine',
            'catalogue': 'Catalogue'
        };

        var CATALOG = {
            '': '',
            'medical-disposible': 'Medical Disposible',
            'medical-equipment': 'Medical Equipment',
            'interactive-flat': 'Interactive Flat Panel Display'
        };

        var CONSULTANT_MANAGEMENT = {
            '': '',
            'consultant': 'Consultant Management',
        };



        var select = $('#category');
        if (select.prop) {
            var options = select.prop('options');
        } else {
            var options = select.attr('options');
        }
        $('option', select).remove();

        if (this.value == "it") {
            // var selectedOption = 'Web';
            $.each(IT, function(val, text) {
                options[options.length] = new Option(text, val);
            });
        } else if (this.value == "website") {
            // var selectedOption = 'Web';
            $.each(WEBSITE, function(val, text) {
                options[options.length] = new Option(text, val);
            });
        } else if (this.value == "medical-device") {
            $.each(MEDICAL_DEVICE, function(val, text) {
                options[options.length] = new Option(text, val);
            });
        } else if (this.value == "catalog") {
            $.each(CATALOG, function(val, text) {
                options[options.length] = new Option(text, val);
            });
        } else if (this.value == 'consultant') {
            $.each(CONSULTANT_MANAGEMENT, function(val, text) {
                options[options.length] = new Option(text, val);
            });
        }
    });
</script>
<script src="https://cdn.ckeditor.com/ckeditor5/11.0.1/classic/ckeditor.js"></script>
<script>
    ClassicEditor.create(document.querySelector('#content'), {
        toolbar: ['Heading', '|', 'bold', 'italic', 'bulletedList', 'numberedList', 'blockQuote', 'Undo', 'Redo', 'Link']
    }).catch(error => {
        console.error(error);
    }).builtinPlugins.map(plugin => plugin.pluginName);
</script>