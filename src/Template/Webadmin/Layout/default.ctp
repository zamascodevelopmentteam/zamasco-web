
<!DOCTYPE html>
<html lang="en">
<head>
  <title><?=$defaultAppSettings['App.Name'];?><?=(!empty($titleModule) ? ' | '.$titleModule : '');?></title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="<?=$defaultAppSettings['App.Name'];?>">
    <meta name="keywords" content="<?=$defaultAppSettings['App.Name'];?>">
    <meta name="author" content="Iqbal Ardiansyah">
    <link rel="shortcut icon" href="<?=$this->Utilities->generateUrlImage(null,$defaultAppSettings['App.Favico']);?>" />
    <!-- Favicon icon -->
    <?php
        $cssExternal = [];
        $cssDefault = [
            'dist/vendors/base/vendors.bundle.css',
            'dist/demo/default/base/style.bundle.css',
        ];

        $cssMain = [

        ];

        $this->Html->css($cssExternal,['block'=>'cssExternal']);
        $this->Html->css($cssDefault,['block'=>'cssDefault','pathPrefix' => '/assets/']);
        $this->Html->css($cssMain,['block'=>'cssMain','pathPrefix' => '/assets/']);

        echo $this->fetch('cssExternal');
        echo $this->fetch('cssDefault');
        echo $this->fetch('cssPlugin');
        echo $this->fetch('cssMain');
    ?>

    <link rel="stylesheet" type="text/css" href="assets/css/styles.css">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
</head>
    <body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
        <?=$this->element('main');?>
        <script src="/assets/ckeditorr/ckeditor.js"></script>
        <script>
            var urlActivities = "<?=$this->Url->build(['controller'=>'Pages','action'=>'activitiesLog']);?>";
        </script>

        <?php
            $jsDefault = [
                'dist/vendors/base/vendors.bundle.js',
                'dist/demo/default/base/scripts.bundle.js',
                'dist/demo/default/custom/crud/metronic-datatable/base/data-ajax.js',
            ];
            $jsMain = [

            ];
            $this->Html->script($jsDefault,[
                'block'=>'jsDefault',
                'pathPrefix' => '/assets/'
                ]
            );
            $this->Html->script($jsDefault,[
                'block'=>'jsPlugin',
                'pathPrefix' => '/assets/'
                ]
            );
            $this->Html->script($jsMain,['block'=>'jsMain','pathPrefix' => '/assets/']);

            echo $this->fetch('jsDefault');
            echo $this->fetch('jsPlugin');
            echo $this->fetch('jsMain');
            echo $this->fetch('script');
        ?>
        <script>

        </script>
    </body>
</html>
