<header>
	<nav class="navbar navbar-default navbar-custom navbar-fixed-top" data-offset-top="200">
		<div class="container">
			<div class="row">
				<div class="navbar-header navbar-header-custom" style="padding: 7.5px 0;">
					<button type="button" class="navbar-toggle collapsed menu-icon" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-logo" href="<?= $this->Url->build(['action' => 'index']) ?>" style="text-align: center;"><img src="assets/img/logos/zamasco.png" alt="logo" style="width: 270px; height: auto; margin-top: 10px;"></a>
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right navbar-links-custom">

						<?php
							$url = $this->request->here;
						?>

						<li><a href="<?= $this->Url->build(['action' => 'index']) ?>">Beranda</a></li>
						<li><a href="<?= $this->Url->build(['action' => 'product']) ?>">Produk</a></li>
						<li><a href="<?= $this->Url->build(['action' => 'portfolio']) ?>">Portofolio</a></li>
						<li><a href="<?= $this->Url->build(['action' => 'gallery']) ?>">Galeri</a></li>
						<li><a href="<?= $this->Url->build(['action' => 'career']) ?>">Career</a></li>
						<li><a href="<?= $this->Url->build(['action' => 'about']) ?>">Tentang</a></li>
						<li><a href="<?= $this->Url->build(['action' => 'contact']) ?>">Kontak Kami</a></li>
					</ul>
				</div>
			</div>
		</div>
	</nav>
</header>