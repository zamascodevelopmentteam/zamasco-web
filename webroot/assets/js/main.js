/*
Author: Revan Pratama
Email: revanp0@gmail.com
*/

"use strict";

$(window).on('load', function () { // makes sure the whole site is loaded
  $('#status').fadeOut(); // will first fade out the loading animation
  $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
  $('body').delay(350).css();
})

$(document).ready(function () {

  $('#testmonials-carousel').owlCarousel({
    loop: false,
    nav: false,
    responsiveClass: true,
    nav: true,
    navText: ["<i class='fa fa-long-arrow-left'></i>", "<i class='fa fa-long-arrow-right'></i>"],
    responsive: {
      0: {
        items: 1,
        nav: false,
        dots: true,
        margin: 10,
      },
      600: {
        items: 1,
        nav: false,
        dots: true,
        margin: 15,
      },
      1000: {
        items: 2,
        dots: false,
        margin: 10,
      }
    }
  })

  $('#project-detail').owlCarousel({
    loop: true,
    nav: false,
    dots: false,
    autoplay: true,
    autoplayTimeout: 3000,
    responsiveClass: true,
    autoplayHoverPause: false,
    responsive: {
      0: {
        items: 1,
        margin: 10,
      },
      600: {
        items: 2,
        margin: 25,
      },
      1000: {
        items: 2,
        margin: 25,
      }
    }
  })

  $('#certificates').owlCarousel({
    loop: true,
    nav: false,
    dots: false,
    autoplay: true,
    autoplayTimeout: 3000,
    responsiveClass: true,
    autoplayHoverPause: false,
    responsive: {
      0: {
        items: 1,
        margin: 15,
      },
      600: {
        items: 3,
        margin: 30,
      },
      1000: {
        items: 4,
        margin: 30,
      }
    }
  })

  $('.countup').counterUp({
    delay: 5,
    time: 2000
  });

  $(function () {
    $('.circlestat').circliful();
  });

  $(".refreshButton").on('click', function (event) {
    location.reload();
  });

  if ($(window).width() > 991) {
    $('ul.nav li.dropdown').hover(function () {
      $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(300);
    }, function () {
      $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(300);
    });
  }

  $(window).scroll(function () {
    var scroll = $(window).scrollTop();
    if ($(window).width() > 991) {
      if (scroll > 30) {
        $(".navbar-custom").css("background", "rgba(255,255,255,0.5)");
        $(".navbar-custom .navbar-links-custom li a").css("color", "rgba(0,0,0,1)")
      }
      else {
        $(".navbar-custom").css("background", "transparent");
        $(".navbar-custom .navbar-links-custom li a").css("color", "rgba(255,255,255,1)")
      }
    }
  })

  $('.tabs_animate').tabslet({
    mouseevent: 'click',
    attribute: 'href',
    animation: true
  });

  $('#videomodal').on('hidden.bs.modal', function () {
    var $this = $(this).find('iframe'),
      tempSrc = $this.attr('src');
    $this.attr('src', "");
    $this.attr('src', tempSrc);
  });

  $('#videomodal').on('hidden.bs.modal', function () {
    var html5Video = document.getElementById("htmlVideo");
    if (html5Video != null) {
      html5Video.pause();
      html5Video.currentTime = 0;
    }
  });

  $('#preloader').fadeOut('normall', function () {
    $(this).remove();
  });



  // $(".scroll-to-top").on('click', function(event){
  //     event.preventDefault();
  //     $("html, body").animate({scrollTop: 0},600);
  // });

  var $grid = $('.isotope-grid').isotope({
    itemSelector: '.isotope-item',
    layoutMode: 'fitRows',
    getSortData: {
      name: '.name',
      symbol: '.symbol',
      number: '.number parseInt',
      category: '[data-category]',
      weight: function (itemElem) {
        var weight = $(itemElem).find('.weight').text();
        return parseFloat(weight.replace(/[\(\)]/g, ''));
      }
    }
  });

  var filterFns = {
    ium: function () {
      var name = $(this).find('.name').text();
      return name.match(/ium$/);
    }
  };

  $('#filters').on('click', 'button', function () {
    var filterValue = $(this).attr('data-filter');
    filterValue = filterFns[filterValue] || filterValue;
    $grid.isotope({ filter: filterValue });
  });

  $('.latest-projects').each(function (i, buttonGroup) {
    var $buttonGroup = $(buttonGroup);
    $buttonGroup.on('click', 'button', function () {
      $buttonGroup.find('.is-checked').removeClass('is-checked');
      $(this).addClass('is-checked');
    });
  });

  new WOW().init();


});

$('#banner').owlCarousel({
  items: 1,
  smartSpeed: 450,
  loop: true,
  autoplay: true,
  autoplayTimeout: 4000,
  autoplayHoverPause: true,
  lazyLoad: true,
  dots: false,
  autoHeight: true
  // autoWidth:true,
});
