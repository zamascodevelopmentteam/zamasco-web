<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 */
Router::defaultRouteClass(DashedRoute::class);
// $routes->connect(
//     '/product/:id',
//     ['controller' => 'Pages', 'action' => 'product'],
//     ['id' => '\d+', 'pass' => ['id']]
// );



Router::scope('/', function (RouteBuilder $routes) {
    $routes->setExtensions(['json', 'xml']);
    $routes->connect('/', ['controller' => 'Pages', 'action' => 'index']);
    $routes->connect('/about', ['controller' => 'Pages', 'action' => 'about']);
    $routes->connect('/product', ['controller' => 'Pages', 'action' => 'product']);
    $routes->connect('/detail', ['controller' => 'Pages', 'action' => 'detail']);
    $routes->connect('/gallery', ['controller' => 'Pages', 'action' => 'gallery']);
    $routes->connect('/career', ['controller' => 'Pages', 'action' => 'career']);
    $routes->connect('/portfolio', ['controller' => 'Pages', 'action' => 'portfolio']);
    $routes->connect('/contact', ['controller' => 'Pages', 'action' => 'contact']);
    $routes->fallbacks(DashedRoute::class);
});

//Router::connect('/details/:id', ['controller' => 'Pages', 'action' => 'product']);

Router::prefix('webadmin', function ($routes) {
    $routes->setExtensions(['json', 'xml']);
    // All routes here will be prefixed with `/admin`
    // And have the prefix => admin route element added.
    $routes->connect('/', ['controller' => 'Dashboard', 'action' => 'index']);
    $routes->connect('/contents', ['controller' => 'Contents', 'action' => 'index']);
    $routes->connect('/documents', ['controller' => 'Documents', 'action' => 'index']);
    $routes->connect('/covers', ['controller' => 'Covers', 'action' => 'index']);
    $routes->connect('/products', ['controller' => 'Products', 'action' => 'index']);
    $routes->connect('/galleries', ['controller' => 'Albums', 'action' => 'index']);
    $routes->connect('/pictures', ['controller' => 'Pictures', 'action' => 'index']);
    $routes->fallbacks(DashedRoute::class);
});
/**
 * Load all plugin routes. See the Plugin documentation on
 * how to customize the loading of plugin routes.
 */
Plugin::routes();
